import {StyleSheet} from 'react-native'

module.exports = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F5F7',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop:30
  },
  containerLogin: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  containerDashboard: {
    flex: 1,
    backgroundColor: '#F4F5F7',
    justifyContent: 'center',
    marginTop:30
  },
  containerProfile: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#F4F5F7',
    justifyContent: 'space-around',
    marginTop:100,
    marginHorizontal: 20
  },
  circle: {
    width: 450,
    height: 450,
    borderRadius: 500 / 2,
    backgroundColor: "#FFF",
    position: "absolute",
    left: 120,
    top: 10
  },
  fixToOptionDouble: {
    marginVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  avatar: {
    borderRadius: 500 / 2,
    width: 35,
    height: 35,
  },
  header: {
    fontWeight: "800",
    fontSize: 22,
    color: "#514E5A"
  },
  subHeadre: {
    fontWeight: "800",
    fontSize: 18,
    color: "#514E5A"
  },
  subTitle: {
    fontWeight: "800",
    fontSize: 12,
    color: "#514E5A"
  },
  descriptionText: {
    fontStyle: 'italic',
    fontSize: 14,
    color: "#514E5A"
  },
  input: {
    marginVertical: 10,
    height: 45,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "#69b58a",
    borderRadius: 30,
    paddingHorizontal: 16,
    color: "#514E5A",
    fontWeight: "600"
  },
  loginButton: {
    height: 45,
    borderRadius: 70 / 2,
    backgroundColor: "#69b58a",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: 'row',
  },
  labelButton: {
    marginHorizontal: 30,
    height: 45,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: 'row',
  },
  loginButtonFacebook: {
    height: 45,
    borderRadius: 70 / 2,
    backgroundColor: "#4267b2",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: 'row',
  },
  optionButton: {
    marginHorizontal: 5,
    height: 40,
    borderRadius: 70 / 2,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: 'row',
  },
  textOptionButton: {
    fontSize:14,
    color:'#69b58a',
    textTransform:'uppercase'
  },
  textOptionButtonReverse: {
    fontSize:14,
    color:'#fff',
    textTransform:'uppercase'
  },
  
  // Card
  containerOptionCard: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  cardSubTitle: {
    marginLeft: 5,
    fontSize: 13,
    fontWeight:'bold'
  },
  cardText: {
    color:'#69b58a'
  }
});