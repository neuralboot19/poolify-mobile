import React from 'react';
import { AsyncStorage, Platform } from 'react-native';

import { AdMobBanner, AdMobInterstitial } from 'expo-ads-admob';

// Material UI to React Native
import { COLOR, ThemeContext, getTheme } from 'react-native-material-ui';

// Globals
import * as globals from './src/util/globals';

// Notification Push
import registerForNotifications from './src/services/PushNotification';

// Navigator
import Router from './src/navigator/Router';
import Customer from './src/navigator/Customer';
import Driver from './src/navigator/Driver';

const uiTheme = {
  palette: {
    primaryColor: "#69b58a",
    accentColor: COLOR.blue500,
  },
  toolbar: {
    container: {
      height: 50,
    },
  },
};

export default class App extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      fontLoaded: false,
      isLoading : true,
      isDriverLogin : false,
      isCustomerLogin : false,
    };
    this.bannerAdId = Platform.OS === 'ios' ? "ca-app-pub-2652743973766752/9234017372" : "ca-app-pub-2652743973766752/7120835838"
    this.interstitialAdId = Platform.OS === 'ios' ? "ca-app-pub-2652743973766752/6224710656" : "ca-app-pub-2652743973766752/1222476049"
  }

  componentDidMount() {
    this.adsMod();
    registerForNotifications();
    AsyncStorage.getItem('loginData').then((item) =>{
      const dataa = JSON.parse(item)
      AsyncStorage.getItem('access_token').then((token) =>{
        if(dataa !== null){
          let user_type = dataa.info.data.type;
          if (user_type === "driver"){
            const data = JSON.parse(item)
            let att = data.info.data.attributes
            globals.access_token = token;
            globals.id = data.info.data.id;
            globals.first_name = att.first_name;
            globals.last_name = att.last_name;
            globals.email = att.email;
            globals.emergencyEmail = att.emergency_email;
            globals.national_id = att.national_id;
            globals.cell_phone = att.cellphone;
            globals.birthday = att.birthday
            globals.status = att.status;
            globals.avatar = att.avatar.url;
            globals.reviews_received = att.reviews_received;
            globals.open_trips = att.open_trips;
            this.setState({isDriverLogin : true})
          }else if(user_type === "customer"){
            const data = JSON.parse(item)
            let att = data.info.data.attributes
            globals.access_token = token;
            globals.id = data.info.data.id;
            globals.first_name = att.first_name;
            globals.last_name = att.last_name;
            globals.email = att.email;
            globals.emergencyEmail = att.emergency_email;
            globals.avatar = att.remote_avatar;
            this.setState({isCustomerLogin : true})
          }
        }else{
          console.log("ALERTA ALERTA ========= AsyncStorage.getItem is NULL ========= ALERTA ALERTA")
        }
      })
    })
    AsyncStorage.multiGet(['password', 'access_token', 'first_name', 'last_name', 'email', 'avatar', 'status'],(error,value) =>{
      if(value[4][1] != null && value[4][1] != undefined){
        globals.access_token = value[1][1] || ""
        globals.first_name = value[2][1] || ""
        globals.last_name = value[3][1] || ""
        globals.email = value[4][1] || ""
        globals.avatar = value[5][1] || ""
        globals.status = value[6][1] || ""
      }
      this.setState({isLoading: true, fontLoaded: true, isDriverLogin: value[4][1] != null})
    })
  }
  
  adsMod = async() => {
    await AdMobInterstitial.setAdUnitID(this.interstitialAdId); // Test ID, Replace with your-admob-unit-id
    await AdMobInterstitial.requestAdAsync({ servePersonalizedAds: false});
    await AdMobInterstitial.showAdAsync();
  }
  
  render() {
    if (this.state.fontLoaded) {
      if (this.state.isDriverLogin == true) {
        return (
          <ThemeContext.Provider value={getTheme(uiTheme)}>
            <Driver />
            <AdMobBanner
              bannerSize="banner"
              adUnitID={this.bannerAdId} // Test ID, Replace with your-admob-unit-id
              servePersonalizedAds={false} />
          </ThemeContext.Provider>
        );
      } else if(this.state.isCustomerLogin == true) {
        return (
          <ThemeContext.Provider value={getTheme(uiTheme)}>
            <Customer />
            <AdMobBanner
              bannerSize="banner"
              adUnitID={this.bannerAdId} // Test ID, Replace with your-admob-unit-id
              servePersonalizedAds={false} />
          </ThemeContext.Provider>
        )
      } else {
        return (
          <ThemeContext.Provider value={getTheme(uiTheme)}>
            <Router />
            <AdMobBanner
              bannerSize="banner"
              adUnitID={this.bannerAdId} // Test ID, Replace with your-admob-unit-id
              servePersonalizedAds={false} />
          </ThemeContext.Provider>
        );
      }
    } else {
      return null
    }
  } 
}