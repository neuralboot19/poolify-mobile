import React from 'react';
import { View, Text, TextInput, Keyboard, ActivityIndicator, TouchableOpacity, Alert } from 'react-native';

// API
import { API } from '../../util/api';

// Globals
import * as globals from '../../util/globals';

// Style
const styles = require('../../../AppStyles');

export default class CustomerUpdateProfile extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      spinner: false,
      firstName: globals.first_name,
      lastName: globals.last_name,
      email: globals.email,
      emergencyEmail: globals.emergencyEmail,
      avatar: globals.avatar,
      phone: globals.phone
    }
  }

  componentDidMount(){
    API.showCustomer(this.showCustomerResponse,{},true);
  }

  showCustomerResponse = {
    success: (response) => {
      try {
        this.setState({
          firstName: response.info.data.attributes.first_name,
          lastName: response.info.data.attributes.last_name,
          email: response.info.data.attributes.email,
          emergencyEmail: response.info.data.attributes.emergency_email,
          avatar: response.info.data.attributes.remote_avatar,
          phone: response.info.data.attributes.cellphone
        })
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Error Cierre su App y inicie de nuevo',err.message,[{text:'OK', onPress: () => this.props.navigation.navigate('Home')}]);
    }
  }

  validateEmail = email => {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  saveParamsProfile = () =>{
    console.log("this.state.phone", this.state.phone.length > 10)
    if (this.state.emergencyEmail == '' || this.state.emergencyEmail == null || this.validateEmail(this.state.emergencyEmail) === false) {
      Alert.alert('Email de Emergencia','No es un correo valido',[{text:'OK'}]);
    }else if (this.state.emergencyEmail == this.state.email) {
      Alert.alert('Email','No puede ser igual a tu email',[{text:'OK'}]);
    }else if (this.state.phone == '' || this.state.phone == null || this.state.phone == 0) {
      Alert.alert('Phone','No puede estar vacío',[{text:'OK'}]);
    }else if (this.state.phone.length > 10) {
      Alert.alert('Phone','No puede ser mas de 10 dígitos',[{text:'OK'}]);
    }else {
      let data = {
        "customer": {
          "emergency_email": this.state.emergencyEmail,
          "cellphone": this.state.phone
        }
      }
      this.setState({ spinner: true });
      API.customerUpdateProfile(this.updateUserResponse,data,true);
    }
  }

  updateUserResponse = {
    success: (response) => {
      try {
        Alert.alert(globals.APP_NAME,response.message,[{
          text:'OK',
          onPress: () => this.setState({
            emergencyEmail: response.info.data.attributes.emergency_email,
            phone: response.info.data.attributes.cellphone,
            spinner: false
          })
        }]);
      } catch (error) {
        Alert.alert(globals.APP_NAME,error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Error Cierre su App y inicie de nuevo',err.message,[{text:'OK', onPress: () => this.props.navigation.navigate('Home')}]);
    }
  }

  setFocus = (textField) =>{
    this[textField].focus()
  }
  
  render(){
    return (
      <View style={[styles.containerProfile,{justifyContent:'space-between',marginTop:80}]}>
        <View style={{marginTop:10}}>
          <Text style={styles.subHeader}>Email de Emergencia</Text>
          <TextInput
            ref={ref => (this.emergencyEmail = ref)}
            style={styles.input}
            onChangeText={(emergencyEmail) => this.setState({ emergencyEmail })}
            value={this.state.emergencyEmail}
            placeholder="Email de Emergencia"
            keyboardType="email-address"
            autoCapitalize="none"
            returnKeyType={"next"}
            onSubmitEditing={() => this.setFocus("phone")}
          />
          <Text style={styles.subHeader}>Phone</Text>
          <TextInput
            ref={ref => (this.phone = ref)}
            style={styles.input}
            onChangeText={phone => this.setState({ phone })}
            value={this.state.phone}
            placeholder="Phone"
            keyboardType="phone-pad"
            secureTextEntry={true}
            returnKeyType={"done"}
            onSubmitEditing={() => Keyboard.dismiss()}
          />
        </View>
        {this.state.spinner == true ? (
          <View style={[styles.loginButton,{marginVertical:10}]}>
            <ActivityIndicator size="small" color="#fff" />
          </View>
        ):(
          <TouchableOpacity style={[styles.loginButton,{marginVertical:10}]} onPress={this.saveParamsProfile}>
            <Text style={styles.textOptionButtonReverse}>Guardar</Text>
          </TouchableOpacity>
        )}
      </View>
    );
  }
}