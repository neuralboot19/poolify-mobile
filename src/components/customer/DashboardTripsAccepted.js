import React from 'react';
import { View, Text, TouchableOpacity, FlatList, Alert } from 'react-native';
import { Icon, Card } from 'react-native-material-ui';
import Moment from 'moment';

// API
import { API } from '../../util/api';

// Globals
import * as globals from '../../util/globals';

// Style
const styles = require('../../../AppStyles');

export default class DashboardTrips extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      acceptedTrips: [],
      isOnRefresh: false,
    };
  }

  componentDidMount(){
    API.listMyTripsCustomer(this.listMyTripsCustomerResponse,true);
  }

  onRefresh = () =>{
    this.setState({isOnRefresh: true, acceptedTrips:[]})
    API.listMyTripsCustomer(this.listMyTripsCustomerResponse,true);
  }

  listMyTripsCustomerResponse = {
    success: (response) => {
      try {
        let defineTrips = Object.entries(response.info)
        if(defineTrips[1][0] == "accepted_trips"){
          let dataAcceptedTrips = (response.info) ? [...this.state.acceptedTrips,...response.info.accepted_trips.data] : this.state.acceptedTrips
          this.setState({acceptedTrips: dataAcceptedTrips || [], isOnRefresh: false})
        }
      } catch (error) {
        Alert.alert(globals.APP_NAME,error.message,[{text:'OK'}])
      }
    },
    error: (err) => {
      Alert.alert('Error Cierre su App y inicie de nuevo',err.message,[{text:'OK', onPress: () => this.props.props.navigation.navigate('Home')}]);
    }
  }

  renderItemTrips = (item) =>{
    let status = ""
    let text = ""
    let styless = []
    let data = item.item
    let carriesParcels = null
    let departureTime = Moment(data.attributes.departure_time).format('DD/MM/YYYY h:mm a')
    if (data.attributes.carries_parcels == true){
      carriesParcels = "SI"
    }else{
      carriesParcels = "NO"
    }
    console.log("data.attributes.statusdata.attributes.statusdata.attributes.status",data.attributes.status)
    switch (data.attributes.status) {
      case "open":
        status = "open"
        text = "Viaje abierto"
        styless = [styles.labelButton,{marginHorizontal:0,backgroundColor:"#69b58a"}]
        break;
      case "in_progress":
        status = "in_progress"
        text = "En progreso"
        styless = [styles.labelButton,{marginHorizontal:0,backgroundColor:"#69b58a"}]
        break;
      case "closed":
        status = "closed"
        text = "Listo para iniciar"
        styless = [styles.labelButton,{marginHorizontal:0,backgroundColor:"#69b58a"}]
        break;
      case "finished":
        status = "finished"  
        text = "Finalizado"
        styless = [styles.labelButton,{marginHorizontal:0,backgroundColor:"red"}]
        break;
    }
    return (
      <Card>
        <TouchableOpacity onPress={() => this.props.props.navigation.navigate("CustomerDetailsTrip",{tripData:data,status:status})}>
          <View style={styless}>
            <Text style={[styles.textOptionButtonReverse, {marginHorizontal:23}]}>{text}</Text>
          </View>
          <View style={styles.fixToOptionDouble}>
            <View style={{marginHorizontal:10}}>
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>CONDUCTOR</Text></Text>
              <Text style={styles.cardSubTitle}>{data.attributes.driver.name || ""}</Text>
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Fecha y hora de salida</Text></Text>
              <Text style={styles.cardSubTitle}>{departureTime}</Text>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft:2 }}>
                <Text style={styles.cardSubTitle} numberOfLines={1}><Text style={styles.cardText}>Puestos: </Text>{data.attributes.seats_available}</Text>
                <Text style={styles.cardSubTitle} numberOfLines={1}><Text style={styles.cardText}>Lleva paquetes: </Text>{carriesParcels}</Text>
              </View>
            </View>
            <View style={styles.containerOptionCard}>
              {data.attributes.status == "in_progress" ? (
                <View style={styles.containerOptionCard}>
                  <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Salimos de: </Text>{data.attributes.origin.name}</Text>
                  <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Vamos a: </Text>{data.attributes.destination.name}</Text>
                  <Text style={styles.header}>{"$" + data.attributes.price_per_seat.toFixed(2)}</Text>
                  <TouchableOpacity style={styles.loginButton} onPress={() => this.buttonPushed(data.id)}>
                    <Text style={[styles.textOptionButtonReverse,{margin:10}]}><Icon color={'#fff'} name={"add-alert"}/></Text>
                  </TouchableOpacity>
                </View>
              ) : (
                <View style={styles.containerOptionCard}>
                  <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Salimos de: </Text>{data.attributes.origin.name}</Text>
                  <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Llegamos a: </Text>{data.attributes.destination.name}</Text>
                  <Text style={styles.header}>{"$" + data.attributes.price_per_seat.toFixed(2)}</Text>
                </View>
              )}
            </View>
          </View>
        </TouchableOpacity>
      </Card>
    )
  }

  buttonPushed(idTrip){
    let data = {"button": {"pushed": true,}}
    API.customerButtonPushed(this.customerButtonPushedResponse,data,idTrip,true);
  }
  
  customerButtonPushedResponse = {
    success: (response) => {
      try {Alert.alert(globals.APP_NAME,response.message,[{text:'OK'}])}
      catch (error) {Alert.alert(globals.APP_NAME,error.message,[{text:'OK'}])}
    },
    error: (err) => {
      Alert.alert(globals.APP_NAME,err.message,[{text:'OK'}])
    }
  }

  ListEmptyComponent = () =>{
    return(
      <View style={{alignItems:'center',justifyContent:'center',paddingVertical:20}}>
        <Text style={styles.descriptionText} >No te han aceptado en ningun viaje.</Text>
      </View>
    )
  }
  
  render(){
    return (
      <FlatList
        data = {this.state.acceptedTrips}
        renderItem = {this.renderItemTrips}
        refreshing={this.state.isOnRefresh}
        onRefresh={this.onRefresh}
        keyExtractor={(item)=>item.id.toString()}
        ListEmptyComponent={this.ListEmptyComponent}
      />
    );
  }
}