import React from 'react';
import { View, Text, TextInput, DrawerLayoutAndroid, TouchableOpacity, FlatList, Alert, AsyncStorage, ActivityIndicator, Modal, Keyboard, Image, Linking } from 'react-native';
import { BottomNavigation, Avatar, Icon, Card } from 'react-native-material-ui';
import Moment from 'moment';

// API
import { API } from '../../util/api';

// Globals
import * as globals from '../../util/globals';

// Components
import DashboardTrips from './DashboardTrips';

// Notification Push
import registerForNotifications from '../../services/PushNotification';

// Style
const styles = require('../../../AppStyles');

export default class CustomerDashboard extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      seatsAvailableTripSelect : 0,
      note : "",
      message : "",
      modalVisible : false,
      spinner: false,
      active: 'home',
      access_token: globals.access_token,
      dataFlatList: [{title: globals.first_name + " " + globals.last_name, key: 'update_profile'}],
      listTrips: [],
      viewTrips: false,
      isOnRefresh: false,
    };
    this.home = this.home.bind(this);
    this.trips = this.trips.bind(this);
    this.settings = this.settings.bind(this);
    this.signOut = this.signOut.bind(this);
  }

  componentDidMount() {
    this.starpermition();
    this.postMobilePushNotificationToken();
    API.listTripsCustomer(this.listTripsCustomerResponse,true);
  }

  starpermition = async() => {
    registerForNotifications();
  }

  postMobilePushNotificationToken = async() => {
    AsyncStorage.getItem('PushNotificationToken').then((item) => {
      globals.mobile_push_token = item
      let data = {
        "user": {
          "mobile_push_token":item,
        }
      }
      API.customerPushNotification(this.customerPushNotificationResponse,data,true);
    })
  }

  customerPushNotificationResponse = {
    success: (response) => {
      try {
        AsyncStorage.getItem('PushNotificationToken').then((item) => {
          globals.mobile_push_token = item
        })
      } catch (error) {
        Alert.alert(globals.APP_NAME,error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Error Cierre su App y inicie de nuevo',err.message,[{text:'OK', onPress: () => this.props.navigation.navigate('Home')}]);
    }
  }

  onRefresh = () =>{
    this.setState({isOnRefresh: true})
    API.listTripsCustomer(this.listTripsCustomerResponse,true);
  }

  listTripsCustomerResponse = {
    success: (response) => {
      try {
        this.setState({listTrips: response.info.data, isOnRefresh: false})
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Error Cierre su App y inicie de nuevo',err.message,[{text:'OK', onPress: () => this.props.navigation.navigate('Home')}]);
    }
  }

  home() {
    this.drawer.closeDrawer();
    this.setState({ active: 'home', viewTrips: false })
    API.listTripsCustomer(this.listTripsCustomerResponse,true);
  }

  trips() {
    this.drawer.closeDrawer();
    this.setState({ active: 'trips', viewTrips: true, listTrips:[] })
  }

  settings() {
    this.drawer.openDrawer();
    this.setState({ active: 'settings', viewTrips: false })
    API.listTripsCustomer(this.listTripsCustomerResponse,true);
  }

  handleView = (item) => {
    if (item.key == "update_profile") {
      this.drawer.closeDrawer();
      this.props.navigation.navigate('CustomerUpdateProfile');
    }
  }

  renderItem = (item) =>{
    var data = item.item
    return(
      <View style={{marginHorizontal: 20}}>
        <TouchableOpacity style={styles.fixToOptionDouble} onPress={()=> this.handleView(item.item)} >
          {globals.avatar == null ? (
            <Avatar size={35} icon="person" iconColor="white" />
          ) : (
            <Image style={styles.avatar} source={{uri:globals.avatar}} />
          )}
          <Text style={{paddingVertical:7}}>{data.title}</Text>
          <Icon style={{paddingVertical:7}} name="keyboard-arrow-right"/>
        </TouchableOpacity> 
      </View>
    )
  }

  toChoose = () =>{
    if(this.state.availableSeats == 0){
      Alert.alert('Available Seats','It cant be empty',[{text:'OK'}]);
    }else if(this.state.note == ""){
      Alert.alert('Note','It cant be empty',[{text:'OK'}]);
    }else{
      Alert.alert(globals.APP_NAME,"¿Seguro que quieres elegir y confirmar este Viaje?",[
        {text: 'No', onPress: () => console.log('Cancel Pressed')},
        {text: 'Si', onPress: () => {
          this.setState({modalVisible : false})
          let data = {
            "proposal": {
              "seats": this.state.availableSeats,
              "notes": this.state.note,
            }
          }
          API.customerPostulateTrips(this.customerPostulateTripsResponse,data,this.state.idTripSelect,true);
        }},
      ])
    }
  }

  customerPostulateTripsResponse = {
    success: (response) => {
      try {
        Alert.alert(globals.APP_NAME,response.message,[
          {text: 'OK', onPress: () => {this.props.navigation.navigate("CustomerDashboard")}},
        ])
      } catch (error) {
        Alert.alert(globals.APP_NAME,JSON.stringify(error.message),[
          {text: 'OK', onPress: () => {this.props.navigation.navigate("CustomerDashboard")}},
        ])
      }
    },
    error: (err) => {
      Alert.alert(globals.APP_NAME,JSON.stringify(err.message),[
        {text: 'OK', onPress: () => {this.props.navigation.navigate("CustomerDashboard")}},
      ])
    }
  }

  calculateSeats = (item) =>{
    if(item <= this.state.seatsAvailableTripSelect){
      this.setState({availableSeats: item, message: ""})
    }else{
      this.setState({availableSeats: 0, message: "Cantidad de puestos no puede ser mayor a la cantidad disponible"})
    }
  }

  displayModal() {
    return(
      <Modal animationType="slide" transparent={true} visible={this.state.modalVisible} onRequestClose={() => console.log("Modal close")}>
        <View style={[styles.containerDashboard,{backgroundColor:'rgba(0,0,0,0.6)',marginTop:0}]}>
          <Card>
            <View style={{margin: 20,}}>
              <Text style={styles.header}>{globals.first_name + " " + globals.last_name}</Text>
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Quedan: </Text>{this.state.seatsAvailableTripSelect} puestos disponibles</Text>
              { this.state.message ? <Text style={styles.cardSubTitle}><Text style={{color:'red'}} >{this.state.message}</Text></Text> : null }
              <TextInput
                style={styles.input}
                onChangeText={item => this.calculateSeats(item)}
                value={this.state.availableSeats}
                placeholder="¿Cuantos puestos quieres?"
                keyboardType="numeric"
                returnKeyType={"next"}
                onSubmitEditing={() => this.setFocus("note")}
              />
              <TextInput
                ref={ref => (this.note = ref)}
                style={styles.input}
                onChangeText={note => this.setState({note : note})}
                value={this.state.note}
                placeholder="Notas adicionales"
                keyboardType="default"
                returnKeyType={"done"}
                onSubmitEditing={() => Keyboard.dismiss()}
              />
              <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                <TouchableOpacity style={styles.loginButton} onPress={() => this.setState({modalVisible : false})}>
                  <Text style={[styles.textOptionButtonReverse, {marginHorizontal:23}]}>Cerrar</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.loginButton} onPress={this.toChoose}>
                  <Text style={[styles.textOptionButtonReverse, {marginHorizontal:23}]}>Aceptar</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Card>
        </View>
      </Modal>
    )
  }

  selectTrip = (trip) => {
    this.setState({modalVisible: true, seatsAvailableTripSelect: trip.attributes.seats_available, idTripSelect: trip.id})
  }

  renderlistTrips = (item) =>{
    let data = item.item
    let carriesParcels = null
    let departureTime = Moment(data.attributes.departure_time).format('DD/MM/YYYY h:mm a')
    if (data.attributes.carries_parcels == true){
      carriesParcels = "SI"
    }else{
      carriesParcels = "NO"
    }
    return(
      <View key={data.id}>
        <Card>
          <TouchableOpacity onPress={() => this.props.navigation.navigate("CustomerDetailsTrip",{tripData:data})}>
            <View style={[styles.labelButton,{marginHorizontal:0,paddingHorizontal:5,backgroundColor:"#69b58a",justifyContent:"space-between"}]}>
              {data.attributes.driver.avatar.url == null ? (
                <Avatar size={35} icon="person" iconColor="white" />
              ) : (
                <Image style={styles.avatar} source={{uri:data.attributes.driver.avatar.url}} />
              )}
              <Text style={[styles.textOptionButtonReverse, {marginHorizontal:23}]}>{data.attributes.driver.name || ""}</Text>
            </View>
            <View style={styles.fixToOptionDouble}>
              <View style={{marginHorizontal:10}}>
                <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Fecha y hora de salida:</Text></Text>
                <Text style={styles.cardSubTitle}>{departureTime}</Text>
                <Text style={styles.cardSubTitle} numberOfLines={1}><Text style={styles.cardText}>Lleva paquetes: </Text>{carriesParcels}</Text>
                <Text style={styles.cardSubTitle} numberOfLines={1}><Text style={styles.cardText}>Puestos: </Text>{data.attributes.seats_available}</Text>
              </View>
              <View style={styles.containerOptionCard}>
                <Text style={[styles.cardSubTitle,{fontSize:16}]}><Text style={styles.cardText}>Salida de: </Text>{data.attributes.origin.name}</Text>
                <Text style={[styles.cardSubTitle,{fontSize:16}]}><Text style={styles.cardText}>Llega a: </Text>{data.attributes.destination.name}</Text>
                <Text style={styles.header}>{"$" + data.attributes.price_per_seat.toFixed(2)}</Text>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.labelButton,{marginHorizontal:0,backgroundColor:"#F4F5F7"}]} onPress={() => this.selectTrip(data)}>
            <Text style={[styles.textOptionButtonReverse, {marginHorizontal:23,color:"#69b58a",fontWeight:"bold"}]}>Elegir viaje</Text>
          </TouchableOpacity>
        </Card>
      </View>
    )
  }

  signOut = async() =>{
    let data = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Token ${this.state.access_token}`
    }
    this.setState({ spinner: true });
    API.signOutCustomer(this.signOutCustomerResponse,{},data)
  }

  signOutCustomerResponse = {
    success: (response) => {
      try {
        AsyncStorage.clear();
        globals.access_token = null
        globals.id = null
        globals.first_name = null
        globals.last_name = null
        globals.email = null
        globals.status = null
        globals.national_id = 0
        globals.cell_phone = 0
        globals.birthday = null
        globals.avatar = null
        globals.reviews_received = null
        globals.open_trips = null
        registerForNotifications();
        this.drawer.closeDrawer();
        this.setState({ active: 'home', spinner: false })
        this.props.navigation.navigate('Home');
      } catch (error) {
        AsyncStorage.clear();
        globals.access_token = null
        globals.id = null
        globals.first_name = null
        globals.last_name = null
        globals.email = null
        globals.status = null
        globals.national_id = 0
        globals.cell_phone = 0
        globals.birthday = null
        globals.avatar = null
        globals.reviews_received = null
        globals.open_trips = null
        registerForNotifications();
        this.setState({spinner: false})
        Alert.alert(globals.APP_NAME,error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      AsyncStorage.clear();
      globals.access_token = null
      globals.id = null
      globals.first_name = null
      globals.last_name = null
      globals.email = null
      globals.status = null
      globals.national_id = 0
      globals.cell_phone = 0
      globals.birthday = null
      globals.avatar = null
      globals.reviews_received = null
      globals.open_trips = null
      registerForNotifications();
      this.setState({spinner: false});
      Alert.alert('Error Cierre su App y inicie de nuevo',err.message,[{text:'OK', onPress: () => this.props.navigation.navigate('Home')}]);
    }
  }

  setFocus = (textField) =>{
    this[textField].focus()
  }
  
  render(){
    let navigationView = (
      <View style={styles.containerLogin}>
        <Text style={[styles.header,{margin:20}]}>Perfil de Usuario</Text>
        <FlatList 
          data = {this.state.dataFlatList}
          renderItem = {this.renderItem}
          keyExtractor={(item)=>item.key.toString()}
        />
        <TouchableOpacity style={[styles.loginButton,{marginHorizontal:10,backgroundColor:'#fff'}]} onPress={()=>{Linking.openURL(`whatsapp://send?phone=+593978973576`)}}>
          <Image style={{width:20, height:20, marginRight:5}} source={require('../../../assets/icon-whatsapp.png')} />
          <Text style={styles.textOptionButton}>Nuestro WhatsApp</Text>
        </TouchableOpacity>
        {this.state.spinner == true ? (
          <View style={[styles.loginButton,{marginVertical:10,marginHorizontal:10}]}>
            <ActivityIndicator size="small" color="#fff" />
          </View>
        ):(
          <TouchableOpacity style={[styles.loginButton,{marginVertical:10,marginHorizontal:10}]} onPress={this.signOut}>
            <Text style={styles.textOptionButtonReverse}>Cerrar Sesión</Text>
          </TouchableOpacity>
        )}
      </View>
    );
    return (
      <View style={styles.containerDashboard}>
        <DrawerLayoutAndroid
          ref={_drawer => (this.drawer = _drawer)}
          drawerWidth={250}
          // drawerPosition={DrawerLayoutAndroid.positions.left}
          renderNavigationView={() => navigationView}
        >
          {this.state.listTrips.length == 0 ? !this.state.viewTrips ?
            <View style={styles.containerDashboard}>
              <View style={styles.circle} />
              <View style={{marginHorizontal: 32}}>
                <View>
                  <Text style={styles.header}>Hi, {globals.first_name + " " + globals.last_name}</Text>
                  <Text style={ styles.subTitle}>No existen viajes disponibles</Text>
                  <Text style={ styles.subTitle}>en este momento.</Text>
                </View>
              </View>
            </View>
            :
            <DashboardTrips props={this.props}/>
            :
            <View style={{flex:1, marginTop:20}}>
              {this.displayModal()}
              <View style={styles.circle} />
              <Text style={[styles.header,{marginHorizontal:10}]}>Viajes Poolify disponibles</Text>
              <View style={{flex:1,marginVertical:10}}>
                <FlatList 
                  data = {this.state.listTrips}
                  renderItem = {this.renderlistTrips}
                  refreshing={this.state.isOnRefresh}
                  onRefresh={this.onRefresh}
                  keyExtractor={(item)=>item.id.toString()}
                />
              </View>
            </View>
          }
        </DrawerLayoutAndroid>
        <BottomNavigation active={this.state.active} hidden={false} >
          <BottomNavigation.Action
            key="settings"
            icon="settings"
            label="Ajustes"
            onPress={() => this.settings()}
          />
          <BottomNavigation.Action
            key="trips"
            icon="local-taxi"
            label="Viajes"
            onPress={() => this.trips()}
          />
          <BottomNavigation.Action
            key="home"
            icon="home"
            label="Inicio"
            onPress={() => this.home()}
          />
        </BottomNavigation>
      </View>
    );
  }
}
