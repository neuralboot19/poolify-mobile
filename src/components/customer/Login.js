import React from 'react';
import { View, Text, ActivityIndicator, TouchableOpacity, AsyncStorage, Alert } from 'react-native';
import * as Facebook from 'expo-facebook';
import { API } from '../../util/api';
import * as globals from '../../util/globals';

// Style
const styles = require('../../../AppStyles');

export default class LoginCustomer extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      spinner: false,
    };
  }

  facebookLogin = async() =>{
    await Facebook.initializeAsync('2344742222455543');
    const { type, token } = await Facebook.logInWithReadPermissionsAsync('2344742222455543', {
      permissions: ["public_profile", "user_friends", "email"],
    });
    if (type === 'success') {
      this.setState({ spinner: true });
      try {
        let data = {
          "customer": {
            "facebook_access_token": token
          }
        }
        API.loginWithFacebook(this.loginWithFacebookResponse,data,true);
      } catch ({message}) {
        Alert.alert('Facebook Login Error:',message,[{text:'OK'}]);
      }
    } else {
      Alert.alert(globals.APP_NAME,'Error logging in. You should try again.',[{text:'OK'}]);
    }
  }

  loginWithFacebookResponse = {
    success: (response, headers) => {
      try {
        AsyncStorage.multiSet([["access_token",JSON.stringify(headers.get('http_authorization'))], ["loginData", JSON.stringify(response)]],()=>{
          let att = response.info.data.attributes;
          globals.access_token = JSON.stringify(headers.get('http_authorization'));
          globals.id = response.info.data.id;
          globals.first_name = att.first_name;
          globals.last_name = att.last_name;
          globals.email = att.email;
          globals.emergencyEmail = att.emergency_email;
          globals.avatar = att.remote_avatar;
          this.props.props.navigation.navigate('CustomerDashboard');
          this.setState({spinner: false});
        });
      } catch (error) {
        this.setState({ spinner: false })
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      this.setState({ spinner: false })
      Alert.alert('Errr',err.message,[{text:'OK'}]);
    }
  }
  
  render(){
    return (
      <View style={{marginTop:20}}>
        <Text style={[styles.header,{marginBottom:10}]}>Sesión Pasajero</Text>
        {this.state.spinner == true ? (
          <View style={styles.loginButtonFacebook}>
            <ActivityIndicator size="small" color="#fff" />
          </View>
        ):(
          <TouchableOpacity style={styles.loginButtonFacebook} onPress={this.facebookLogin}>
            <Text style={styles.textOptionButtonReverse}>Continúa con facebook</Text>
          </TouchableOpacity>
        )}
      </View>
    );
  }
}