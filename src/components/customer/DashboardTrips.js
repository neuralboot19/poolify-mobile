import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-material-ui';

// Globals
import * as globals from '../../util/globals';

// Style
const styles = require('../../../AppStyles');

// Components
import DashboardTripsAccepted from './DashboardTripsAccepted';
import DashboardTripsCancelled from './DashboardTripsCancelled';
import DashboardTripsChosen from './DashboardTripsChosen';

export default class DashboardTrips extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      isCallapseOpen: false,
      isCallapseOpen1: true,
      isCallapseOpen2: true,
    };
  }

  optionTab(optionTab) {
    if(optionTab.tab == "open_trips") {
      this.setState({isCallapseOpen: !this.state.isCallapseOpen, isCallapseOpen1: true, isCallapseOpen2: true})
    }else if(optionTab.tab == "accepted_trips") {
      this.setState({isCallapseOpen: true, isCallapseOpen1: !this.state.isCallapseOpen1, isCallapseOpen2: true})
    }else if(optionTab.tab == "cancelled_trips") {
      this.setState({isCallapseOpen: true, isCallapseOpen1: true, isCallapseOpen2: !this.state.isCallapseOpen2})
    }
  }
  
  render(){
    return (
      <View style={{flex: 1, marginTop:20}}>
        <View style={styles.circle} />
        <Text style={[styles.header,{marginHorizontal:10}]}>Tus viajes</Text>
        <View style={[styles.fixToOptionDouble,{marginHorizontal:10,marginVertical:0}]}>
          <TouchableOpacity style={styles.fixToOptionDouble} onPress={() => this.optionTab({tab:'open_trips'})}>
            <Text style={!this.state.isCallapseOpen ? {color:'#69b58a'} : null}>Elegidos</Text>
            <Icon color={!this.state.isCallapseOpen ? '#69b58a' : null} name={(this.state.isCallapseOpen) ? "arrow-drop-down" : "arrow-drop-up"}/>
          </TouchableOpacity>
          <TouchableOpacity style={styles.fixToOptionDouble} onPress={() => this.optionTab({tab:'accepted_trips'})}>
            <Text style={!this.state.isCallapseOpen1 ? {color:'#69b58a'} : null}>Aceptados</Text>
            <Icon color={!this.state.isCallapseOpen1 ? '#69b58a' : null} name={(this.state.isCallapseOpen1) ? "arrow-drop-down" : "arrow-drop-up"}/>
          </TouchableOpacity>
          <TouchableOpacity style={styles.fixToOptionDouble} onPress={() => this.optionTab({tab:'cancelled_trips'})}>
            <Text style={!this.state.isCallapseOpen2 ? {color:'#69b58a'} : null}>Cancelados</Text>
            <Icon color={!this.state.isCallapseOpen2 ? '#69b58a' : null} name={(this.state.isCallapseOpen2) ? "arrow-drop-down" : "arrow-drop-up"}/>
          </TouchableOpacity>
        </View>
        {globals.emergencyEmail == null || global.emergencyEmail == "" ? (
          <View style={[styles.containerLogin,{marginHorizontal:10}]}>
            <Text></Text>
            <Text>Debes actulizar tus datos de Emergencia para continuar.</Text>
            {this.state.spinner ? (
              <View style={[styles.loginButton,{marginVertical:10}]}>
                <ActivityIndicator size="small" color="#fff" />
              </View>
            ):(
              <TouchableOpacity style={[styles.loginButton,{marginVertical:10}]} onPress={() => this.props.props.navigation.navigate('CustomerUpdateProfile')}>
                <Text style={styles.textOptionButtonReverse}>Ir actulizar</Text>
              </TouchableOpacity>
            )}
          </View>
        ) : (
          <View>
            {!this.state.isCallapseOpen ? (
              <DashboardTripsChosen props={this.props.props} />
            ) : null}
            {!this.state.isCallapseOpen1 ? (
              <DashboardTripsAccepted props={this.props.props} />
            ) : null}
            {!this.state.isCallapseOpen2 ? (
              <DashboardTripsCancelled props={this.props.props} />
            ) : null}
          </View>
        )}
      </View>
    );
  }
}