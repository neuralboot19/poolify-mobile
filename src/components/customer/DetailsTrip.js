import React from 'react';
import { View, Text, TouchableOpacity, ScrollView, Modal, TextInput, Keyboard, Alert, FlatList, KeyboardAvoidingView } from 'react-native';
import { Icon, Card, Divider, Avatar } from 'react-native-material-ui';
import { AirbnbRating } from 'react-native-ratings';
import Collapsible from 'react-native-collapsible';
import Moment from 'moment';

// API
import { API } from '../../util/api';

// Globals
import * as globals from '../../util/globals';

// Style
const styles = require('../../../AppStyles');

export default class CustomerDetailsTrip extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      isCallapseOpen: false,
      isCallapseOpen1: true,
      departureTime: "",
      locationOriginName: "",
      locationDestinationName: "",
      travelTime: 0,
      seatsAvailable: 0,
      carriesParcels: null,
      pricePerSeat: 0,
      tripId: null,
      statusTrip: null,
      modalVisible: false,
      availableSeats: 0,
      note: "",
      message: "",
      dateMessages: [],
      canReview: [],
      sendTextComment: "",
      modalReviewVisible : false,
      sendReviewComment: "",
      sendReviewStar: 3
    };
    this.ratingCompleted = this.ratingCompleted.bind(this);
  }

  componentDidMount(){
    API.listTripCustomer(this.listTripCustomerResponse,this.props.route.params.tripData.id,true)
    API.listMessagesCustomer(this.listMessagesCustomerResponse,this.props.route.params.tripData.id,true);
    API.canReview(this.canReviewCustomerResponse,this.props.route.params.tripData.id,true)
  }

  listTripCustomerResponse = {
    success: (response) => {
      try {
        this.setState({
          departureTime : JSON.stringify(response.info.data.attributes.departure_time),
          locationOriginName : response.info.data.attributes.origin.name,
          locationDestinationName: response.info.data.attributes.destination.name,
          travelTime: JSON.stringify(response.info.data.attributes.travel_time),
          seatsAvailable: JSON.stringify(response.info.data.attributes.seats_available),
          carriesParcels: JSON.stringify(response.info.data.attributes.carries_parcels),
          pricePerSeat: JSON.stringify(response.info.data.attributes.price_per_seat),
          tripId: response.info.data.id,
          statusTrip: response.info.data.attributes.status
        })
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Error Cierre su App y inicie de nuevo',err.message,[{text:'OK', onPress: () => this.props.navigation.navigate('Home')}]);
    }
  }

  listMessagesCustomerResponse = {
    success: (response) => {
      try {
        this.setState({
          dateMessages : response.info.data,
        })
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      console.log("Comment Trip without comments err",err);
    }
  }

  canReviewCustomerResponse = {
    success: (response) => {
      try {
        this.setState({
          canReview: response.info.data.can_review,
        })
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      console.log("Can Review Trip err",err);
    }
  }

  calculateSeats = (item) =>{
    if(item <= this.state.seatsAvailable){
      this.setState({availableSeats: item, message: ""})
    }else{
      this.setState({availableSeats: 0, message: "Cantidad de puestos no puede ser mayor a la cantidad disponible"})
    }
  }

  toChoose = () =>{
    if(this.state.availableSeats == 0){
      Alert.alert('Available Seats','It cant be empty',[{text:'OK'}]);
    }else if(this.state.note == ""){
      Alert.alert('Note','It cant be empty',[{text:'OK'}]);
    }else{
      Alert.alert(globals.APP_NAME,"¿Seguro que quieres elegir y confirmar este Viaje?",[
        {text: 'No', onPress: () => console.log('Cancel Pressed')},
        {text: 'Si', onPress: () => {
          this.setState({modalVisible : false})
          let data = {
            "proposal": {
              "seats": this.state.availableSeats,
              "notes": this.state.note,
            }
          }
          API.customerPostulateTrips(this.customerPostulateTripsResponse,data,this.state.tripId,true);
        }},
      ])
    }
  }

  customerPostulateTripsResponse = {
    success: (response) => {
      try {
        Alert.alert(globals.APP_NAME,response.message,[
          {text: 'OK', onPress: () => {this.props.navigation.navigate("CustomerDashboard")}},
        ])
      } catch (error) {
        Alert.alert(globals.APP_NAME,JSON.stringify(error.message),[
          {text: 'OK', onPress: () => {this.props.navigation.navigate("CustomerDashboard")}},
        ])
      }
    },
    error: (err) => {
      Alert.alert(globals.APP_NAME,JSON.stringify(err.message),[
        {text: 'OK', onPress: () => {this.props.navigation.navigate("CustomerDashboard")}},
      ])
    }
  }

  displayModal() {
    return(
      <Modal animationType="slide" transparent={true} visible={this.state.modalVisible} onRequestClose={() => console.log("Modal close")}>
        <View style={[styles.containerDashboard,{backgroundColor:'rgba(0,0,0,0.3)',marginTop:0}]}>
          <Card>
            <View style={{margin: 20,}}>
              <Text style={styles.header}>{globals.first_name + " " + globals.last_name}</Text>
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Quedan: </Text>{this.state.seatsAvailable} puestos disponibles</Text>
              { this.state.message ? <Text style={styles.cardSubTitle}><Text style={{color:'red'}} >{this.state.message}</Text></Text> : null }
              <TextInput
                style={styles.input}
                onChangeText={item => this.calculateSeats(item)}
                value={this.state.availableSeats}
                placeholder="¿Cuantos puestos quieres?"
                keyboardType="numeric"
                returnKeyType={"next"}
                onSubmitEditing={() => this.setFocus("note")}
              />
              <TextInput
                ref={ref => (this.note = ref)}
                style={styles.input}
                onChangeText={note => this.setState({note : note})}
                value={this.state.note}
                placeholder="Notas adicionales"
                keyboardType="default"
                returnKeyType={"done"}
                onSubmitEditing={() => Keyboard.dismiss()}
              />
              <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                <TouchableOpacity style={styles.loginButton} onPress={() => this.setState({modalVisible : false})}>
                  <Text style={[styles.textOptionButtonReverse, {marginHorizontal:23}]}>Cerrar</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.loginButton} onPress={this.toChoose}>
                  <Text style={[styles.textOptionButtonReverse, {marginHorizontal:23}]}>Aceptar</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Card>
        </View>
      </Modal>
    )
  }

  displayReviewModal() {
    return(
      <Modal animationType="slide" transparent={true} visible={this.state.modalReviewVisible} onRequestClose={() => console.log("Modal close")}>
        <View style={[styles.containerDashboard,{backgroundColor:'rgba(0,0,0,0.6)',marginTop:0}]}>
          <Card>
            <View style={{marginVertical:20}}>
              <View style={{marginHorizontal: 20, alignItems: 'center'}}>
                <Avatar size={90} icon="person" iconColor="white" image={globals.avatar} />
              </View>
              <View style={{marginHorizontal: 20}}>
                <AirbnbRating
                  count={5}
                  reviews={['Terrible', 'Bad', 'Okay', 'Good', 'Great']}
                  defaultRating={3}
                  onFinishRating={this.ratingCompleted}
                />
                <TextInput
                  ref={ref => (this.sendReviewComment = ref)}
                  style={[styles.input,{marginHorizontal:0}]}
                  onChangeText={sendReviewComment => this.setState({sendReviewComment : sendReviewComment})}
                  value={this.state.sendReviewComment}
                  placeholder={"Que opinas de " + this.props.route.params.tripData.attributes.driver.name + "?"}
                  keyboardType="default"
                  returnKeyType={"done"}
                  onSubmitEditing={() => Keyboard.dismiss()}
                />
                <TouchableOpacity style={[styles.loginButton,{marginHorizontal:0}]} onPress={() => this.tapSendReview()}>
                  <Text style={[styles.textOptionButtonReverse, {marginHorizontal:23}]}>Enviar calificación</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Card>
        </View>
      </Modal>
    )
  }

  ratingCompleted(rating) {
    this.setState({sendReviewStar: rating})
  }

  tapSendReview() {
    if(this.state.sendReviewComment == ""){
      Alert.alert(globals.APP_NAME,'It cant be empty',[{text:'OK'}]);
    }else{
      let data = {
        "review" : {
          "qualification" : this.state.sendReviewStar,
          "comment" : this.state.sendReviewComment
        }
      }
      this.setState({modalReviewVisible: false})
      API.customerSendReview(this.customerSendReviewResponse,data,this.props.route.params.tripData.id,true);
    }
  }

  customerSendReviewResponse = {
    success: (response) => {
      try {
        Alert.alert("Poolify",response.message,[{text: 'OK', onPress: () => this.props.navigation.navigate('CustomerDashboard')}])
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK', onPress: () => this.props.navigation.navigate('CustomerDashboard')}]);
      }
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK', onPress: () => this.props.navigation.navigate('CustomerDashboard')}]);
    }
  }

  renderItem = (item) =>{
    var data = item.item
    return (
      <View style={{marginVertical:10}}>
        <Text style={[styles.cardSubTitle,{fontWeight:"800" ,fontSize:18, marginLeft:0}]}>
          <Text style={styles.cardText}>{data.attributes.owner.type == "Customer" ? "Usuario" : "Conductor"} </Text>{data.attributes.owner.full_name}
        </Text>
        <Text style={styles.descriptionText}>{data.attributes.message}</Text>
      </View>
    )
  }

  ItemSeparatorComponent = () =>{
    return(
      <Divider/>
    )
  }

  sendComment = () =>{
    if(this.state.sendTextComment == ""){
      Alert.alert(globals.APP_NAME,'It cant be empty',[{text:'OK'}]);
    }else{
      let data = {
        "message": {
          "message": this.state.sendTextComment
        }
      }
      API.createdMessagesCustomer(this.createdMessagesCustomerResponse,data,this.props.route.params.tripData.id,true);
    }
  }

  createdMessagesCustomerResponse = {
    success: (response) => {
      try {
        Alert.alert(globals.APP_NAME,response.message,[{text:'OK', onPress: () => this.updateResponseMessages(response) }]);
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK'}]);
    }
  }

  updateResponseMessages = (response) => {
    this.note.clear()
    let newDate = this.state.dateMessages.map((d)=>d)
    newDate.push(response.info.data)
    this.setState({dateMessages: newDate, sendTextComment:""})
  }

  ListEmptyComponent = () =>{
    return(
      <Text style={[styles.descriptionText,{marginHorizontal:16, scaleY:-1}]} >Sin Comentarios</Text>
    )
  }

  tapOnCancle() {
    Alert.alert(globals.APP_NAME,"¿Confirma retirarce del viaje?",[
      {text: 'NO', onPress: () => console.log('Cancel Pressed')},
      {text: 'SI', onPress: () => API.customerQuitTrip(this.customerQuitTripResponse,this.props.route.params.tripData.id,true)}
    ])
  }

  customerQuitTripResponse = {
    success: (response) => {
      try {
        Alert.alert(globals.APP_NAME,response.message,[{text: 'OK', onPress: () => {
          const {setTripRemove} = this.props.route.params
          setTripRemove()
          this.props.navigation.goBack()
        }}])
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK'}]);
    }
  }

  setFocus = (textField) =>{
    this[textField].focus()
  }
  
  render(){
    let departureTime = Moment(new Date(this.state.departureTime.replace(/"/g, ""))).utcOffset(-5).format('DD/MM/YYYY h:mm a')
    return (
      <View style={[styles.containerProfile,{marginTop:80,marginHorizontal:10}]}>
        {this.displayModal()}
        {this.displayReviewModal()}
        <View style={styles.fixToOptionDouble}>
          <TouchableOpacity onPress={() => this.setState({isCallapseOpen: !this.state.isCallapseOpen, isCallapseOpen1: true})}>
            <View style={[styles.optionButton,{justifyContent:'space-between',backgroundColor: "#69b58a",width:140}]}>
              <Text style={[styles.textOptionButtonReverse,{marginHorizontal:10}]}>Detalles</Text>
              <Icon style={{color:'#fff'}} name={(this.state.isCallapseOpen) ? "arrow-drop-down" : "arrow-drop-up"}/>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress= {() => this.setState({isCallapseOpen: true, isCallapseOpen1: !this.state.isCallapseOpen1})}>
            <View style={[styles.optionButton,{justifyContent:'space-between',backgroundColor: "#69b58a",width:140}]}>
              <Text style={[styles.textOptionButtonReverse,{marginHorizontal:10}]}>Comentarios</Text>
              <Icon style={{color:'#fff'}} name={(this.state.isCallapseOpen1) ? "arrow-drop-down" : "arrow-drop-up"}/>
            </View>
          </TouchableOpacity>
        </View>
        <ScrollView bounces={false}>
          <Collapsible collapsed={this.state.isCallapseOpen} duration={200}>
            <View style={[styles.fixToOptionDouble,{marginHorizontal: 32, marginVertical: 10}]}>
              <View>
                <Text style={[styles.cardSubTitle,{fontSize:14}]}><Text style={styles.cardText}>Fecha y hora de salida</Text></Text>
                <Text style={[styles.cardSubTitle,{fontSize:14}]}><Text style={styles.subText}>{departureTime}</Text></Text>
                <Text style={[styles.cardSubTitle,{fontSize:14,marginTop:10}]}><Text style={styles.cardText}>Destino</Text></Text>
                <Text style={[styles.cardSubTitle,{fontSize:14}]}><Text style={styles.subText}>Desde: {this.state.locationOriginName}</Text></Text>
                <Text style={[styles.cardSubTitle,{fontSize:14}]}><Text style={styles.subText}>Hasta: {this.state.locationDestinationName}</Text></Text>
                <Text style={[styles.cardSubTitle,{fontSize:14,marginTop:10}]}><Text style={styles.cardText}>Tiempo estimo de llegada</Text></Text>
                <Text style={[styles.cardSubTitle,{fontSize:14}]}><Text style={styles.subText}>Aproximadamente: {this.state.travelTime + "HS"}</Text></Text>
                <Text style={[styles.cardSubTitle,{fontSize:14,marginTop:10}]}><Text style={styles.cardText}>Puestos disponibles </Text>{this.state.seatsAvailable}</Text>
                <Text style={[styles.cardSubTitle,{fontSize:14,marginTop:10}]}><Text style={styles.cardText}>Llevo paquetes </Text>{!this.state.carriesParcels ? "NO" : "SI"}</Text>
              </View>
              <View style={styles.containerOptionCard}>
                <Text style={[styles.header,{fontSize:40}]}>{"$ " + this.state.pricePerSeat}</Text>
              </View>
            </View>
          </Collapsible>
          <Collapsible collapsed={this.state.isCallapseOpen1} duration={200}>
            <View style={{marginHorizontal: 32, marginVertical: 10}}>
              <FlatList
                data={this.state.dateMessages}
                renderItem={this.renderItem}
                ItemSeparatorComponent={this.ItemSeparatorComponent}
                keyExtractor={(item)=>item.id.toString()}
                ListEmptyComponent={this.ListEmptyComponent}
                inverted
              />
            </View>
          </Collapsible>          
        </ScrollView>
        {!this.state.isCallapseOpen1 && this.props.route.params.status == "open_trips" || !this.state.isCallapseOpen1 && this.props.route.params.status == "closed" ? (
          this.state.statusTrip == "in_progress" || this.state.statusTrip == "finished" ? (
            <View>
              {this.props.route.params.status == "accepted_trips" ? (
                <TouchableOpacity style={[styles.loginButton,{marginVertical:10}]} onPress={() => this.setState({modalReviewVisible: true})}>
                  <Text style={styles.textOptionButtonReverse}>Calificar</Text>
                </TouchableOpacity>
              ) : null}
            </View>
          ) : (
            <KeyboardAvoidingView style={{flex:1}} behavior="padding">
              <TextInput
                ref={ref => (this.note = ref)}
                style={[styles.input,{height:80}]}
                onChangeText={note => this.setState({sendTextComment : note})}
                value={this.state.sendTextComment}
                placeholder="Realiza una pregunta."
                keyboardType="default"
                multiline={true}
                maxLength={150}
                returnKeyType={"done"}
                onSubmitEditing={() => Keyboard.dismiss()}
              />
              <TouchableOpacity style={[styles.loginButton,{marginVertical:10}]} onPress={() => this.sendComment()}>
                <Text style={styles.textOptionButtonReverse}>Enviar</Text>
              </TouchableOpacity>
            </KeyboardAvoidingView>
          )
        ) : (
          <View>
            {this.props.route.params.status == "finished" && this.state.canReview ? (
              <TouchableOpacity style={[styles.loginButton,{marginVertical:10}]} onPress={() => this.setState({modalReviewVisible: true})}>
                <Text style={styles.textOptionButtonReverse}>Calificar</Text>
              </TouchableOpacity>
            ) : null}
            {this.props.route.params.status == "open_trips" ? (
              <TouchableOpacity style={[styles.loginButton,{marginVertical:10}]} onPress={() => this.tapOnCancle()}>
                <Text style={styles.textOptionButtonReverse}>Retirar del Viaje</Text>
              </TouchableOpacity>
            ) :null}
            {this.props.route.params.status == undefined ? (
              <TouchableOpacity style={[styles.loginButton,{marginVertical:10}]} onPress={() => this.setState({modalVisible: true})}>
                <Text style={styles.textOptionButtonReverse}>Elegir viaje</Text>
              </TouchableOpacity>
            ) : null}
          </View>
        )}
      </View>
    );
  }
}