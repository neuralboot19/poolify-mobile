import React from 'react';
import { View, Text, TouchableOpacity, FlatList, Alert } from 'react-native';
import { Card } from 'react-native-material-ui';
import Moment from 'moment';

// API
import { API } from '../../util/api';

// Globals
import * as globals from '../../util/globals';

// Style
const styles = require('../../../AppStyles');

export default class DashboardTrips extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      openTrips: [],
      isOnRefresh: false,
    };
  }

  componentDidMount(){
    API.listMyTripsCustomer(this.listMyTripsCustomerResponse,true);
  }

  onRefresh = () =>{
    this.setState({isOnRefresh: true, openTrips:[]})
    API.listMyTripsCustomer(this.listMyTripsCustomerResponse,true);
  }

  setTripRemove = () => {
    this.setState({ openTrips: [] })
    API.listMyTripsCustomer(this.listMyTripsCustomerResponse,true);
  }

  listMyTripsCustomerResponse = {
    success: (response) => {
      try {
        let defineTrips = Object.entries(response.info)
        if(defineTrips[0][0] == "open_trips"){
          let dataOpenTrips = (response.info) ? [...this.state.openTrips,...response.info.open_trips.data] : this.state.openTrips
          this.setState({openTrips: dataOpenTrips || [], isOnRefresh: false})
        }
      } catch (error) {
        Alert.alert(globals.APP_NAME,error.message,[{text:'OK'}])
      }
    },
    error: (err) => {
      Alert.alert('Error Cierre su App y inicie de nuevo',err.message,[{text:'OK', onPress: () => this.props.props.navigation.navigate('Home')}]);
    }
  }

  renderItemTrips = (item) =>{
    let status = ""
    let text = ""
    let styless = []
    let data = item.item
    let carriesParcels = null
    let departureTime = Moment(data.attributes.departure_time).format('DD/MM/YYYY h:mm a')
    if (data.attributes.carries_parcels == true){
      carriesParcels = "SI"
    }else{
      carriesParcels = "NO"
    }
    switch (data.attributes.status) {
      case "open":
        status = "open_trips"
        text = "Abierto"
        styless = [styles.labelButton,{marginHorizontal:0,backgroundColor:"#69b58a"}]
        break;
      case "cancelled":
        status = "cancelled"
        text = "Cancelado"
        styless = [styles.labelButton,{marginHorizontal:0,backgroundColor:"red"}]
        break;
    }
    return (
      <Card>
        <TouchableOpacity onPress={() => data.attributes.status == "open" ? this.props.props.navigation.navigate("CustomerDetailsTrip",{setTripRemove:this.setTripRemove, tripData:data, status:status}) : ""}>
          <View style={styless}>
            <Text style={[styles.textOptionButtonReverse, {marginHorizontal:23}]}>{text}</Text>
          </View>
          <View style={styles.fixToOptionDouble}>
            <View style={{marginHorizontal:10}}>
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>CONDUCTOR</Text></Text>
              <Text style={styles.cardSubTitle}>{data.attributes.driver.name || ""}</Text>
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Fecha y hora de salida</Text></Text>
              <Text style={styles.cardSubTitle}>{departureTime}</Text>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft:2 }}>
                <Text style={styles.cardSubTitle} numberOfLines={1}><Text style={styles.cardText}>Puestos: </Text>{data.attributes.seats_available}</Text>
                <Text style={styles.cardSubTitle} numberOfLines={1}><Text style={styles.cardText}>Lleva paquetes: </Text>{carriesParcels}</Text>
              </View>
            </View>
            <View style={styles.containerOptionCard}>
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Salgo de: </Text>{data.attributes.origin.name}</Text>
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Puedes ir a: </Text>{data.attributes.destination.name}</Text>
              <Text style={styles.header}>{"$" + data.attributes.price_per_seat.toFixed(2)}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </Card>
    )
  }

  ListEmptyComponent = () =>{
    return(
      <View style={{alignItems:'center',justifyContent:'center',paddingVertical:20}}>
        <Text style={styles.descriptionText} >No has elegido ningun viaje.</Text>
      </View>
    )
  }
  
  render(){
    return (
      <FlatList
        data = {this.state.openTrips}
        renderItem = {this.renderItemTrips}
        refreshing={this.state.isOnRefresh}
        onRefresh={this.onRefresh}
        keyExtractor={(item)=>item.id.toString()}
        ListEmptyComponent={this.ListEmptyComponent}
      />
    );
  }
}