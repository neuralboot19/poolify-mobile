import React from 'react';
import { View, Text, DrawerLayoutAndroid, TouchableOpacity, FlatList, Alert, AsyncStorage, ActivityIndicator, Image, Linking } from 'react-native';
import { BottomNavigation, Avatar, Icon } from 'react-native-material-ui';

// API
import { API } from '../../util/api';

// Globals
import * as globals from '../../util/globals';

// Component
import DashboardHome from './DashboardHome';
import DashboardTrips from './DashboardTrips';

// Notification Push
import registerForNotifications from '../../services/PushNotification';

// Style
const styles = require('../../../AppStyles');

export default class DriverDashboard extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      spinner: false,
      active: 'home',
      dataFlatList: [{title: globals.first_name + " " + globals.last_name, key: 'update_profile'},{title: "Comentarios", key : 'comment'},{title: "Tus autos",key : 'cars'}],
      viewTrips: false,
      viewHome: true,
      tripList: [],
      inProgressTrip: []
    };
    this.home = this.home.bind(this);
    this.trips = this.trips.bind(this);
    this.settings = this.settings.bind(this);
    this.signOut = this.signOut.bind(this);
  }

  componentDidMount(){
    this.starpermition();
    this.alertEmergency();
    this.postMobilePushNotificationToken();
    if(globals.status != "on_hold"){
      API.listTripsDriver(this.listTripsDriverResponse,{},true);
    }
  }

  starpermition = async() => {
    registerForNotifications();
  }

  alertEmergency = async() => {
    if(globals.emergencyEmail == null || globals.emergencyEmail == ""){
      Alert.alert("SEGURIDAD " + globals.APP_NAME,"Por seguridad para sus viajes en progreso, debe completar sus datos de emergencia",[{text:'OK', onPress: () => this.props.navigation.navigate('DriverUpdateProfile')}]);
    }
  }

  updateDataFlatList = (data) => {
    this.setState({dataFlatList:[{title: data.first_name + " " + data.last_name, key: 'update_profile'},{title: "Comentarios", key : 'comment'},{title: "Tus autos",key : 'cars'}]})
  }

  postMobilePushNotificationToken = async() => {
    AsyncStorage.getItem('PushNotificationToken').then((item) => {
      globals.mobile_push_token = item
      let data = {
        "user": {
          "mobile_push_token":item,
        }
      }
      API.driverPushNotification(this.driverPushNotificationResponse,data,true);
    })
  }

  driverPushNotificationResponse = {
    success: (response) => {
      try {
        AsyncStorage.getItem('PushNotificationToken').then((item) => {
          globals.mobile_push_token = item
        })
      } catch (error) {
        Alert.alert(globals.APP_NAME,error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Error Cierre su App y inicie de nuevo',err.message,[{text:'OK', onPress: () => this.props.navigation.navigate('Home')}]);
    }
  }

  finishedTrip(id) {
    API.driverfinishedTrip(this.driverfinishedTripResponse,id,true);
  }

  driverfinishedTripResponse = {
    success: (response) => {
      try {
        Alert.alert(globals.APP_NAME,response.message,[{text: 'OK', onPress:() => {this.home()}}])
      } catch (error) {
        Alert.alert(globals.APP_NAME,error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Error Cierre su App y inicie de nuevo',err.message,[{text:'OK', onPress: () => this.props.navigation.navigate('Home')}]);
    }
  }

  listTripsDriverResponse = {
    success: (response) => {
      try {
        let newTripData = (response.info.open_trips) ? [...this.state.tripList,...response.info.open_trips.data] : this.state.tripList
        let inProgressTrip = response.info.in_progress_trips.data
        this.setState({
          tripList: newTripData,
          inProgressTrip: inProgressTrip
        })
      } catch (error) {
        Alert.alert(globals.APP_NAME,error.message,[{text:'OK'}])
      }
    },
    error: (err) => {
      Alert.alert('Error Cierre su App y inicie de nuevo',err.message,[{text:'OK', onPress: () => this.props.navigation.navigate('Home')}]);
    }
  }

  setTripNew = (tripData) => {
    let newDate = this.state.tripList.map((tl)=>tl)
    newDate.push(tripData.info.data)
    this.setState({tripList: newDate.reverse()})
  }

  home() {
    this.drawer.closeDrawer();
    this.setState({ active: 'home', viewTrips: false, viewHome: true, tripList: [], inProgressTrip: [] })
    API.listTripsDriver(this.listTripsDriverResponse,{},true);
  }

  trips() {
    this.drawer.closeDrawer();
    this.setState({ active: 'trips', viewTrips: true, viewHome: false })
  }

  alertTrips() {
    Alert.alert(globals.APP_NAME,"Debe finalizar su viaje en progreso",[{text:'OK'}]);
  }

  settings() {
    this.drawer.openDrawer();
    this.setState({ active: 'settings', viewTrips: false, viewHome: true, tripList: [], inProgressTrip: [] })
    API.listTripsDriver(this.listTripsDriverResponse,{},true);
  }

  handleView = (item) => {
    if (item.key == "update_profile") {
      this.drawer.closeDrawer();
      this.props.navigation.navigate('DriverUpdateProfile',{updateDataFlatList:this.updateDataFlatList});
    } else if (item.key == "comment") {
      this.drawer.closeDrawer();
      this.props.navigation.navigate('DriverReviews');
    } else if (item.key == "cars") {
      this.drawer.closeDrawer();
      this.props.navigation.navigate('DriverCars');
    }
  }

  renderItem = (item) =>{
    var data = item.item
    return(
      <View style={{marginHorizontal: 20}}>
        <TouchableOpacity style={styles.fixToOptionDouble} onPress={()=> this.handleView(item.item)} >
          {data.key == "update_profile" ? (
            globals.avatar == null ? (
              <Avatar size={35} icon="person" iconColor="white" />
            ) : (
              <Image style={styles.avatar} source={{uri:globals.avatar}} />
            )
          ) : null}
          <Text style={{paddingVertical:7}}>{data.title}</Text>
          <Icon style={{paddingVertical:7}} name="keyboard-arrow-right"/>
        </TouchableOpacity> 
      </View>
    )
  }

  signOut = async() =>{
    let data = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Token ${this.state.access_token}`
    }
    this.setState({ spinner: true });
    API.signOutCustomer(this.signOutCustomerResponse,{},data)
  }

  signOutCustomerResponse = {
    success: (response) => {
      try {
        AsyncStorage.clear();
        globals.access_token = null
        globals.id = null
        globals.first_name = null
        globals.last_name = null
        globals.email = null
        globals.emergencyEmail = null
        globals.status = null
        globals.national_id = 0
        globals.cell_phone = 0
        globals.avatar = null
        globals.reviews_received = null
        globals.open_trips = null
        registerForNotifications();
        this.drawer.closeDrawer();
        this.setState({ active: 'home', spinner: false })
        this.props.navigation.navigate('Home');
      } catch (error) {
        AsyncStorage.clear();
        globals.access_token = null
        globals.id = null
        globals.first_name = null
        globals.last_name = null
        globals.email = null
        globals.emergencyEmail = null
        globals.status = null
        globals.national_id = 0
        globals.cell_phone = 0
        globals.birthday = null
        globals.avatar = null
        globals.reviews_received = null
        globals.open_trips = null
        registerForNotifications();
        this.drawer.closeDrawer();
        this.setState({ active: 'home', spinner: false })
        this.props.navigation.navigate('Home');      
        Alert.alert(globals.APP_NAME,error.message,[{text:'OK'}])
      }
    },
    error: (err) => {
      AsyncStorage.clear();
      globals.access_token = null
      globals.id = null
      globals.first_name = null
      globals.last_name = null
      globals.email = null
      globals.status = null
      globals.national_id = 0
      globals.cell_phone = 0
      globals.birthday = null
      globals.avatar = null
      globals.reviews_received = null
      globals.open_trips = null
      registerForNotifications();
      this.setState({spinner: false});
      Alert.alert('Error Cierre su App y inicie de nuevo',err.message,[{text:'OK', onPress: () => this.props.navigation.navigate('Home')}]);
    }
  }

  viewInProgress() {
    let id = this.state.inProgressTrip.map((t) => t.id)
    let origin = this.state.inProgressTrip.map((t) => t.attributes.origin.name)
    let destination = this.state.inProgressTrip.map((t) => t.attributes.destination.name)
    let price = this.state.inProgressTrip.map((t) => t.attributes.price_per_seat)
    let carriesParcels = this.state.inProgressTrip.map((t) => t.attributes.carries_parcels)
    if (carriesParcels){
      carriesParcels = "SI"
    }else{
      carriesParcels = "NO"
    }
    return (
      <View style={[styles.containerLogin,{marginTop:20,marginHorizontal:10}]}>
        <View style={styles.circle} />
        <Text style={styles.header}>Viaje en progreso</Text>
        <View style={{alignItems:'flex-start',marginTop:10}}>
          <View style={styles.fixToOptionDouble}>
            <Text style={[styles.cardSubTitle,{width:120,marginLeft:0}]}><Text style={styles.cardText}>Sali de</Text></Text>
            <Text style={[styles.cardSubTitle,{width:120,marginLeft:0}]}><Text style={styles.cardText}>Voy a</Text></Text>
            <Text style={[styles.cardSubTitle,{width:120,marginLeft:0}]}><Text style={styles.cardText}>Lleva paquetes</Text></Text>
          </View>
          <View style={styles.fixToOptionDouble}>
            <Text style={[styles.cardSubTitle,{width:120,marginLeft:0}]}>{origin}</Text>
            <Text style={[styles.cardSubTitle,{width:120,marginLeft:0}]}>{destination}</Text>
            <Text style={[styles.cardSubTitle,{width:120,marginLeft:0}]}>{carriesParcels}</Text>
          </View>
        </View>
        <View style={styles.containerOptionCard}>
          <Text style={[styles.header,{fontSize:60}]}>{"$" + price}</Text>
          <Text style={styles.header}>Precio por pasajero</Text>
        </View>
        <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
          {this.state.spinner == true ? (
            <View style={[styles.loginButton,{marginVertical:10,width:300}]}>
              <ActivityIndicator size="small" color="#fff" />
            </View>
          ):(
            <TouchableOpacity style={[styles.loginButton,{marginVertical:10,width:300}]} onPress={() => this.finishedTrip(id)}>
              <Text style={styles.textOptionButtonReverse}>Finalizar Viaje</Text>
            </TouchableOpacity>
          )}
          <TouchableOpacity style={styles.loginButton} onPress={() => this.buttonPushed(id)}>
            <Text style={[styles.textOptionButtonReverse,{margin:10}]}><Icon color={'#fff'} name={"add-alert"}/></Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  buttonPushed(idTrip){
    let data = {"button": {"pushed": true,}}
    API.driverButtonPushed(this.driverButtonPushedResponse,data,idTrip,true);
  }
  
  driverButtonPushedResponse = {
    success: (response) => {
      try {Alert.alert(globals.APP_NAME,response.message,[{text:'OK'}])}
      catch (error) {Alert.alert(globals.APP_NAME,error.message,[{text:'OK'}])}
    },
    error: (err) => {
      Alert.alert('Error Cierre su App y inicie de nuevo',err.message,[{text:'OK', onPress: () => this.props.navigation.navigate('Home')}]);
    }
  }
  
  render(){
    let navigationView = (
      <View style={styles.containerLogin}>
        <Text style={[styles.header,{margin:20}]}>Perfil de conductor</Text>
        <FlatList 
          data = {this.state.dataFlatList}
          renderItem = {this.renderItem}
          keyExtractor={(item)=>item.key.toString()}
        />
        <TouchableOpacity style={[styles.loginButton,{marginHorizontal:10,backgroundColor:'#fff'}]} onPress={()=>{Linking.openURL(`whatsapp://send?phone=+593978973576`)}}>
          <Image style={{width:20, height:20, marginRight:5}} source={require('../../../assets/icon-whatsapp.png')} />
          <Text style={styles.textOptionButton}>Nuestro WhatsApp</Text>
        </TouchableOpacity>
        {this.state.spinner == true ? (
          <View style={[styles.loginButton,{marginVertical:10,marginHorizontal:10}]}>
            <ActivityIndicator size="small" color="#fff" />
          </View>
        ):(
          <TouchableOpacity style={[styles.loginButton,{marginVertical:10,marginHorizontal:10}]} onPress={this.signOut}>
            <Text style={styles.textOptionButtonReverse}>Cerrar Sesión</Text>
          </TouchableOpacity>
        )}
      </View>
    );
    return (
      <View style={styles.containerDashboard}>
        <DrawerLayoutAndroid
          ref={_drawer => (this.drawer = _drawer)}
          drawerWidth={250}
          // drawerPosition={DrawerLayoutAndroid.positions.Left}
          renderNavigationView={() => navigationView}
        >
          {this.state.viewTrips ? (
            <DashboardTrips props={this.props}/>
          ) :null}
          {this.state.inProgressTrip.length > 0 && this.state.viewHome ? (
            this.viewInProgress()
          ) : (
            this.state.viewHome ? (
              <View style={[styles.containerDashboard,{marginHorizontal:0,marginTop:20}]}>
                {globals.status != "on_hold" ? (
                  <View style={[styles.fixToOptionDouble,{marginVertical:0}]}>
                    <Text style={[styles.header,{marginHorizontal:10}]}>Viajes</Text>
                    <TouchableOpacity style={[styles.fixToOptionDouble,{marginVertical:0}]} onPress={() => (globals.emergencyEmail == null || globals.emergencyEmail == "") ? this.alertEmergency() : this.props.navigation.navigate("DriverCreateTrip",{setTripNew: this.setTripNew})}>
                      <Text style={[styles.header,{color:'#69b58a'}]}>Crear</Text>
                      <Icon color="#69b58a" size={34} name="chevron-right"/>
                    </TouchableOpacity>
                  </View>
                ) :null}
                <DashboardHome setTripAll={this.home} props={this.props} />
              </View>
            ) :null
          )}
        </DrawerLayoutAndroid>
        <BottomNavigation active={this.state.active} hidden={false} >
          <BottomNavigation.Action
            key="settings"
            icon="settings"
            label="Ajustes"
            onPress={() => this.settings()}
          />
          <BottomNavigation.Action
            key="trips"
            icon="local-taxi"
            label="Viajes"
            onPress={() => this.state.inProgressTrip.length > 0 ? this.alertTrips() : this.trips()}
          />
          <BottomNavigation.Action
            key="home"
            icon="home"
            label="Inicio"
            onPress={() => this.home()}
          />
        </BottomNavigation>
      </View>
    );
  }
}
