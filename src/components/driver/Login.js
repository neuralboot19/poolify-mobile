import React from 'react';
import { View, Text, Switch, TextInput, ActivityIndicator, TouchableOpacity, AsyncStorage, Keyboard, Alert } from 'react-native';
import { API } from '../../util/api';
import * as globals from '../../util/globals';

// Style
const styles = require('../../../AppStyles');

export default class LoginDriver extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      passwordConfirmation: '',
      spinner: false,
      isEnabledSwitchCreatedAcount: false,
    };
  }

  logInDriver = async() =>{
    if (this.state.email === '') {
      Alert.alert('Error de validación','El campo de correo no puede estar vacío',[{text:'OK'}]);
    } else if (this.state.password === '') {
      Alert.alert('Error de validación','Por favor ingrese su contraseña',[{text:'OK'}]);
    } else {
      let data = {
        "driver": {
          "email": this.state.email,
          "password": this.state.password
        }
      }
      this.setState({ spinner: true });
      API.loginDriver(this.loginDriverResponse,data,true);
    }
  }

  loginDriverResponse = {
    success: (response, headers) => {
      try {
        AsyncStorage.multiSet([['access_token', JSON.stringify(headers.get('http_authorization')) || ''],['loginData', JSON.stringify(response)]],()=>{
          let att = response.info.data.attributes;
          globals.access_token = JSON.stringify(headers.get('http_authorization'));
          globals.id = response.info.data.id;
          globals.first_name = att.first_name;
          globals.last_name = att.last_name;
          globals.email = att.email;
          globals.status = att.status;
          globals.cell_phone = att.cellphone;
          globals.emergencyEmail = att.emergency_email;
          globals.national_id = att.national_id;
          globals.birthday = att.birthday;
          globals.avatar = att.avatar.url;
          globals.reviews_received = att.reviews_received;
          globals.open_trips = att.open_trips;
          this.props.props.navigation.navigate('DriverDashboard');
        })
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
        this.setState({ spinner: false })
      }
      this.setState({ spinner: false })
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK'}]);
      this.setState({ spinner: false })
    }
  }

  validateEmail = email => {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  signInDriver = async() =>{
    if (this.state.firstName === '') {
      Alert.alert('Nombre','No puede estar vacío',[{text:'OK'}]);
    } else if (this.state.lastName === '') {
      Alert.alert('Apellido','No puede estar vacío',[{text:'OK'}]);
    } else if (this.validateEmail(this.state.email) === false) {
      Alert.alert('E-mail','No es un correo valido',[{text:'OK'}]);
    } else if (this.state.password === '') {
      Alert.alert('Contraseña','No puede estar vacío',[{text:'OK'}]);
    } else if (this.state.password !== this.state.passwordConfirmation) {
      Alert.alert('Confirmación de contraseña','Las contraseñas no coinciden',[{text:'OK'}]);
    } else {
      let data = {
        "driver": {
          "first_name": this.state.firstName,
          "last_name": this.state.lastName,
          "email": this.state.email,
          "password": this.state.password,
          "password_confirmation": this.state.passwordConfirmation,
        }
      }
      this.setState({ spinner: true });
      API.driverSignUp(this.driverSignUpResponse,data,true);
    }
  }

  driverSignUpResponse = {
    success: (response, headers) => {
      try {
        AsyncStorage.multiSet([['access_token', JSON.stringify(headers.get('http_authorization'))],['loginData', JSON.stringify(response)]],()=>{
          let att = response.info.data.attributes;
          globals.access_token = JSON.stringify(headers.get('http_authorization'));
          globals.id = response.info.data.id;
          globals.first_name = att.first_name;
          globals.last_name = att.last_name;
          globals.email = att.email;
          globals.status = att.status;
          globals.emergencyEmail = att.emergency_email;
          globals.national_id = att.national_id;
          globals.cell_phone = att.cellphone;
          globals.birthday = att.birthday;
          globals.avatar = att.avatar.url;
          globals.reviews_received = att.reviews_received;
          globals.open_trips = att.open_trips;
          this.props.props.navigation.navigate('DriverDashboard');
        })
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK', onPress: () => this.setState({ spinner: false }) }]);
      }
      this.setState({ spinner: false });
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK', onPress: () => this.setState({ spinner: false }) }]);
    }
  }

  toggleSwitch = () => {
    if (!this.state.isEnabledSwitchCreatedAcount) {
      this.setState({isEnabledSwitchCreatedAcount: true})
    } else {
      this.setState({isEnabledSwitchCreatedAcount: false})
    }
  }

  setFocus = (textField) =>{
    this[textField].focus()
  }
  
  render(){
    return (
      <View style={{marginTop:20}}>
        <Text style={[styles.header,{marginBottom:10}]}>{this.state.isEnabledSwitchCreatedAcount ? 'Registro Conductor' : 'Sesión Conductor'}</Text>
        <TextInput
          style={styles.input}
          onChangeText={email => this.setState({ email })}
          value={this.state.email}
          placeholder="User / Email"
          keyboardType="email-address"
          autoCapitalize="none"
          returnKeyType={"next"}
          onSubmitEditing={() => this.setFocus("passwordInput")}
        />
        <TextInput
          ref={ref => (this.passwordInput = ref)}
          style={styles.input}
          onChangeText={password => this.setState({ password })}
          value={this.state.password}
          placeholder="Contraseña"
          secureTextEntry={true}
          autoCapitalize="none"
          returnKeyType={this.state.isEnabledSwitchCreatedAcount ? "next" : "done"}
          onSubmitEditing={() => this.state.isEnabledSwitchCreatedAcount ? this.setFocus("passwordConfirmationInput") : Keyboard.dismiss()}
        />
        {this.state.isEnabledSwitchCreatedAcount ? (
          <View>
            <TextInput
              ref={ref => (this.passwordConfirmationInput = ref)}
              style={styles.input}
              onChangeText={passwordConfirmation => this.setState({ passwordConfirmation })}
              value={this.state.passwordConfirmation}
              placeholder="Confirmación de contraseña"
              secureTextEntry={true}
              returnKeyType={"next"}
              onSubmitEditing={() => this.setFocus("firstName")}
            />
            <TextInput
              ref={ref => (this.firstName = ref)}
              style={styles.input}
              onChangeText={firstName => this.setState({ firstName })}
              value={this.state.firstName}
              placeholder="Nombre"
              keyboardType="default"
              returnKeyType={"next"}
              onSubmitEditing={() => this.setFocus("lastName")}
            />
            <TextInput
              ref={ref => (this.lastName = ref)}
              style={styles.input}
              onChangeText={lastName => this.setState({ lastName })}
              value={this.state.lastName}
              placeholder="Apellido"
              keyboardType="default"
              returnKeyType={"done"}
              onSubmitEditing={() => Keyboard.dismiss()}
            />
          </View>
        ) : null}
        {this.state.spinner == true ? (
          <View style={styles.loginButton}>
            <ActivityIndicator size="small" color="#fff" />
          </View>
        ):(
          <TouchableOpacity style={styles.loginButton} onPress={this.state.isEnabledSwitchCreatedAcount ? this.signInDriver : this.logInDriver}>
            <Text style={styles.textOptionButtonReverse}>{this.state.isEnabledSwitchCreatedAcount ? 'Crear Cuenta' : 'Iniciar sesión'}</Text>
          </TouchableOpacity>
        )}
        <View style={[styles.fixToOptionDouble,{justifyContent:'flex-end'}]}>
          <TouchableOpacity style={styles.optionButton} onPress={() => this.toggleSwitch()}>
            <Text style={this.state.isEnabledSwitchCreatedAcount ? styles.textOptionButton : [styles.textOptionButton,{color:'#3e3e3e'}] }>
              {this.state.isEnabledSwitchCreatedAcount ? 'INICIA SESION' : 'REGISTRATE'}
            </Text>
          </TouchableOpacity>
          <Switch
            primary
            trackColor={{ false: "#767577", true: "#69b58a" }}
            thumbColor={"#f4f3f4"}
            ios_backgroundColor="#3e3e3e"
            onValueChange={this.toggleSwitch}
            value={this.state.isEnabledSwitchCreatedAcount}
          />
        </View>
      </View>
    );
  }
}
