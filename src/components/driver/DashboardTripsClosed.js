import React from 'react';
import { View, Text, TouchableOpacity, FlatList, Alert } from 'react-native';
import { Card } from 'react-native-material-ui';
import Moment from 'moment';

// API
import { API } from '../../util/api';

// Globals
import * as globals from '../../util/globals';

// Style
const styles = require('../../../AppStyles');

export default class DashboardTrips extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      closedTrips: [],
      isOnRefresh: false,
    };
  }

  componentDidMount(){
    API.listTripsDriver(this.listTripsDriverResponse,{},true);
  }

  onRefresh = () =>{
    this.setState({isOnRefresh: true, closedTrips:[]})
    API.listTripsDriver(this.listTripsDriverResponse,{},true);
  }

  listTripsDriverResponse = {
    success: (response) => {
      try {
        this.setState({
          closedTrips: response.info.closed_trips.data,
          isOnRefresh: false
        })
      } catch (error) {
        Alert.alert(globals.APP_NAME,error.message,[{text:'OK'}])
      }
    },
    error: (err) => {
      Alert.alert('Error Cierre su App y inicie de nuevo',err.message,[{text:'OK', onPress: () => this.props.props.navigation.navigate('Home')}]);
    }
  }

  renderItem = (item) =>{
    let data = item.item
    let carriesParcels = null
    let departureTime = Moment.utc(data.attributes.departure_time).format('DD/MM/YYYY h:mm a')
    if (data.attributes.carries_parcels){
      carriesParcels = "SI"
    }else{
      carriesParcels = "NO"
    }
    return (
      <Card>
        <View style={[styles.labelButton,{marginHorizontal:0,backgroundColor:"#69b58a"}]}>
          <Text style={[styles.textOptionButtonReverse, {marginHorizontal:23}]}>Viaje listo para iniciar</Text>
        </View>
        <TouchableOpacity onPress={() => this.props.props.navigation.navigate("DriverDetailsTrip",{tripData:data})}>
          <View style={[styles.fixToOptionDouble,{marginVertical:0}]}>
            <View style={{margin:8}}>
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Fecha y hora de salida: </Text></Text>
              <Text style={styles.cardSubTitle}>{departureTime}</Text>
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Salgo de: </Text>{data.attributes.origin.name}</Text>
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Voy a: </Text>{data.attributes.destination.name}</Text>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft:2 }}>
                <Text style={styles.cardSubTitle} numberOfLines={1}><Text style={styles.cardText}>Puestos: </Text>{data.attributes.seats_available}</Text>
                <Text style={styles.cardSubTitle} numberOfLines={1}><Text style={styles.cardText}>Lleva paquetes: </Text>{carriesParcels}</Text>
              </View>
            </View>
            <View style={styles.containerOptionCard}>
              <Text style={styles.header}>{"$" + data.attributes.price_per_seat.toFixed(2)}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </Card>
    )
  }

  ListEmptyComponent = () =>{
    return(
      <View style={{flex:1,alignItems:'center',justifyContent:'center',paddingVertical:20}}>
        <Text style={styles.descriptionText} >No tienes viajes cerrados</Text>
      </View>
    )
  }
  
  render(){
    return (
      <FlatList 
        data = {this.state.closedTrips}
        renderItem = {this.renderItem}
        refreshing={this.state.isOnRefresh}
        onRefresh={this.onRefresh}
        keyExtractor={(item)=>item.id.toString()}
        ListEmptyComponent={this.ListEmptyComponent}
      />
    );
  }
}