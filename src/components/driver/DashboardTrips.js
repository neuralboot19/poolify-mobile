import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-material-ui';

// Style
const styles = require('../../../AppStyles');

// Components
import TripsFinished from './DashboardTripsFinished';
import TripsCancelled from './DashboardTripsCancelled';
import TripsClosed from './DashboardTripsClosed';

export default class DashboardTrips extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      isCallapseOpen: false,
      isCallapseOpen1: true,
      isCallapseOpen2: true,
      isCallapseOpen3: true,
    };
  }

  render(){
    return (
      <View style={{flex: 1, marginTop:20,marginHorizontal:0}}>
        <View style={styles.circle} />
        <Text style={[styles.header,{marginHorizontal:10}]}>Tus viajes</Text>
        <View style={[styles.fixToOptionDouble,{marginHorizontal:10,marginVertical:0}]}>
          <TouchableOpacity style={styles.fixToOptionDouble} onPress={() => this.setState({isCallapseOpen: !this.state.isCallapseOpen, isCallapseOpen1: true, isCallapseOpen2: true})}>
            <Text style={!this.state.isCallapseOpen ? {color:'#69b58a'} : null}>Cerrados</Text>
            <Icon color={!this.state.isCallapseOpen ? '#69b58a' : null} name={(this.state.isCallapseOpen) ? "arrow-drop-down" : "arrow-drop-up"}/>
          </TouchableOpacity>
          <TouchableOpacity style={styles.fixToOptionDouble} onPress= {() => this.setState({isCallapseOpen: true, isCallapseOpen1: !this.state.isCallapseOpen1, isCallapseOpen2: true})}>
            <Text style={!this.state.isCallapseOpen1 ? {color:'#69b58a'} : null}>Finalizados</Text>
            <Icon color={!this.state.isCallapseOpen1 ? '#69b58a' : null} name={(this.state.isCallapseOpen1) ? "arrow-drop-down" : "arrow-drop-up"}/>
          </TouchableOpacity>
          <TouchableOpacity style={styles.fixToOptionDouble} onPress= {() => this.setState({isCallapseOpen: true, isCallapseOpen1: true, isCallapseOpen2: !this.state.isCallapseOpen2})}>
            <Text style={!this.state.isCallapseOpen2 ? {color:'#69b58a'} : null}>Cancelados</Text>
            <Icon color={!this.state.isCallapseOpen2 ? '#69b58a' : null} name={(this.state.isCallapseOpen2) ? "arrow-drop-down" : "arrow-drop-up"}/>
          </TouchableOpacity>
        </View>
        {!this.state.isCallapseOpen ? (
          <TripsClosed props={this.props.props} />
        ) : null}
        {!this.state.isCallapseOpen1 ? (
          <TripsFinished props={this.props.props} />
        ) : null}
        {!this.state.isCallapseOpen2 ? (
          <TripsCancelled props={this.props.props} />
        ) : null}
      </View>
    );
  }
}