import React from 'react';
import { View, Text, FlatList, Image } from 'react-native';
import { Avatar } from 'react-native-material-ui';

// Globals
import * as globals from '../../util/globals';

// Style
const styles = require('../../../AppStyles');

export default class DriverReviews extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      data: globals.reviews_received
    };
  }

  renderItem = (item) =>{
    var data = item.item
    return (
      <View style={{marginHorizontal: 20, marginVertical:10}}>
        <View style={[styles.fixToOptionDouble,{alignItems:'center',justifyContent:'flex-start'}]}>
          {data.reviewer.avatar.url == null ? (
            <View><Avatar size={35} icon="person" iconColor="white" /></View>
          ) : (
            <Image style={styles.avatar} source={{uri:data.reviewer.avatar.url}} />
          )}
          <Text style={[styles.subHeadre,{marginHorizontal:10}]}>{data.reviewer.name}</Text>
        </View>
        <View>
          <Text style={styles.descriptionText}>{'Comentario: ' + data.comment}</Text>
          <Text style={styles.descriptionText}>{'Calificación: ' + data.qualification}</Text>
        </View>
      </View>
    )
  }

  ListEmptyComponent = () =>{
    return(
      <View style={{marginHorizontal:32,marginVertical:10}} >
        <Text style={styles.subTitle}>Sin Comentarios</Text>
      </View>
    )
  }
  
  render(){
    return (
      <View style={{marginTop:80}}>
        <FlatList 
          data = {this.state.data}
          renderItem = {this.renderItem}
          keyExtractor={(item)=>item.id.toString()}
          ListEmptyComponent={this.ListEmptyComponent}
        />
      </View>
    );
  }
}