import React from 'react';
import { View, Text, TextInput, Keyboard, ActivityIndicator, TouchableOpacity, Alert, ScrollView, AsyncStorage } from 'react-native';
import DateTime from "react-native-modal-datetime-picker";
import Moment from 'moment';

// API
import { API } from '../../util/api';

// Globals
import * as globals from '../../util/globals';

// Style
const styles = require('../../../AppStyles');

export default class DriverUpdateProfile extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      spinner: false,
      firstName: globals.first_name,
      lastName: globals.last_name,
      email: globals.email,
      emergencyEmail: globals.emergencyEmail,
      nationalId: globals.national_id,
      phone: globals.cell_phone,
      birthday: globals.birthday
    }
  }

  validateEmail = email => {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  saveParamsProfile = () =>{
    if (this.state.firstName === '' || this.state.firstName === null) {
      Alert.alert('Nombre','No puede estar vacío',[{text:'OK'}]);
    } else if (this.state.lastName === '' || this.state.lastName === null) {
      Alert.alert('Apellido','No puede estar vacío',[{text:'OK'}]);
    } else if (this.state.emergencyEmail === '' || this.state.emergencyEmail === null || this.validateEmail(this.state.emergencyEmail) === false) {
      Alert.alert('E-mail de seguridad','No es un correo valido',[{text:'OK'}]);
    } else if (this.state.nationalId.length === undefined || this.state.nationalId.length < 10 || this.state.nationalId.length > 13) {
      Alert.alert('Identification','No puede estar vacío y debe tener entre 10 y 13 dígitos.',[{text:'OK'}]);
    } else if (this.state.phone.length === undefined || this.state.phone.length < 10 || this.state.phone.length > 10 ) {
      Alert.alert('Teléfono','Debe tener 10 dígitos.',[{text:'OK'}]);
    } else if (this.state.birthday === 'AAAA-MM-DD' || this.state.birthday === null) {
      Alert.alert('Cumpleaños','Debe seleccionar fecha de cumpleaños',[{text:'OK'}]);
    } else {
      let data = {
        "driver": {
          "first_name": this.state.firstName,
          "last_name": this.state.lastName,
          "emergency_email": this.state.emergencyEmail,
          "national_id": this.state.nationalId,
          "cellphone": this.state.phone,
          "birthday": this.state.birthday
        }
      }
      this.setState({ spinner: true });
      API.driverUpdate(this.driverUpdateResponse,data,true);
    }
  }

  driverUpdateResponse = {
    success: (response, headers) => {
      try {
        Alert.alert(globals.APP_NAME,response.message,[{text: 'OK', onPress: () => {
          AsyncStorage.multiSet([['access_token', JSON.stringify(headers.get('http_authorization')) || ''],['loginData', JSON.stringify(response)]],()=>{
            globals.access_token = JSON.stringify(headers.get('http_authorization')) || '';
            globals.id = response.info.data.id || '';
            globals.first_name = response.info.data.attributes.first_name;
            globals.last_name = response.info.data.attributes.last_name;
            globals.email = response.info.data.attributes.email;
            globals.emergencyEmail = response.info.data.attributes.emergency_email;
            globals.national_id = response.info.data.attributes.national_id;
            globals.status = response.info.data.attributes.status;
            globals.cell_phone = response.info.data.attributes.cellphone;
            globals.birthday = response.info.data.attributes.birthday;
            globals.avatar = response.info.data.attributes.avatar.url;
            const {updateDataFlatList} = this.props.route.params
            updateDataFlatList(response.info.data.attributes)
            this.props.navigation.goBack()
          })
        }}])
      } catch (error) {
        Alert.alert(globals.APP_NAME,error.message,[{ text:'OK', onPress: () => this.setState({ spinner: false }) }]);
      }
    },
    error: (err) => {
      Alert.alert(globals.APP_NAME,err.message,[{ text:'OK', onPress: () => this.setState({ spinner: false }) }]);
    }
  }

  showDatePicker = () => {console.log("Estoy en fecha"), this.setState({isDateTimePickerVisible: true})};

  hideDateTimePicker = () => this.setState({isDateTimePickerVisible: false});

  handleDatePicked = (date) => {
    let datePicked = Moment(new Date(date)).format("YYYY-MM-DD");
    let m = Moment().day(-6575)
    if(Moment(new Date(date)).format("YYYY") <= m.format("YYYY")) {
      this.setState({birthday: datePicked});
      this.hideDateTimePicker();
    }else{
      this.hideDateTimePicker();
      Alert.alert(globals.APP_NAME,'Debes ser mayor de 18 años.',[{text:'OK', onPress: () => this.showDatePicker()}]);
    }
  }

  setFocus = (textField) =>{
    this[textField].focus()
  }
  
  render(){
    return (
      <View style={styles.containerProfile}>
        <ScrollView>
          <TextInput
            style={styles.input}
            onChangeText={(firstName) => this.setState({ firstName })}
            value={this.state.firstName}
            placeholder="Nombre"
            keyboardType="default"
            returnKeyType={"next"}
            onSubmitEditing={() => this.setFocus("lastName")}
          />
          <TextInput
            ref={ref => (this.lastName = ref)}
            style={styles.input}
            onChangeText={(lastName) => this.setState({ lastName })}
            value={this.state.lastName}
            placeholder="Apellido"
            keyboardType="default"
            returnKeyType={"next"}
            onSubmitEditing={() => this.setFocus("emergencyEmail")}
          />
          <TextInput
            ref={ref => (this.email = ref)}
            style={styles.input}
            onChangeText={(email) => this.setState({ email })}
            value={this.state.email}
            placeholder="E-mail"
            keyboardType="email-address"
            returnKeyType={"next"}
            editable={false}
            onSubmitEditing={() => this.setFocus("emergencyEmail")}
          />
          <TextInput
            ref={ref => (this.emergencyEmail = ref)}
            style={styles.input}
            onChangeText={(emergencyEmail) => this.setState({ emergencyEmail })}
            value={this.state.emergencyEmail}
            placeholder="E-mail de Seguridad"
            keyboardType="email-address"
            autoCapitalize="none"
            returnKeyType={"next"}
            onSubmitEditing={() => this.setFocus("nationalId")}
          />
          <TextInput
            ref={ref => (this.nationalId = ref)}
            style={styles.input}
            onChangeText={nationalId => this.setState({ nationalId })}
            value={this.state.nationalId}
            placeholder="Identificación"
            keyboardType="numeric"
            returnKeyType={"next"}
            onSubmitEditing={() => this.setFocus("phone")}
          />
          <TextInput
            ref={ref => (this.phone = ref)}
            style={styles.input}
            onChangeText={phone => this.setState({ phone })}
            value={this.state.phone}
            placeholder="Teléfono"
            keyboardType="phone-pad"
            secureTextEntry={true}
            returnKeyType={"done"}
            onSubmitEditing={() => Keyboard.dismiss()}
          />
          <View style={{marginHorizontal: 16, marginTop:10 }}>
            <Text style={styles.subHeader}>Cumpleaños</Text>
          </View>
          <TouchableOpacity style={styles.input} onPress={this.showDatePicker}>
            <Text style={{marginVertical:10}} >{this.state.birthday}</Text>
          </TouchableOpacity>
          <DateTime
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this.handleDatePicked}
            onCancel={this.hideDateTimePicker}
            mode='date'
          />
        </ScrollView>
        {this.state.spinner == true ? (
          <View style={[styles.loginButton,{marginVertical:10}]}>
            <ActivityIndicator size="small" color="#fff" />
          </View>
        ):(
          <TouchableOpacity style={[styles.loginButton,{marginVertical:10}]} onPress={this.saveParamsProfile}>
            <Text style={styles.textOptionButtonReverse}>Actualizar</Text>
          </TouchableOpacity>
        )}
      </View>
    );
  }
}