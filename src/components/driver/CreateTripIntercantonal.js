import React from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator, ScrollView, Alert, TextInput, Keyboard } from 'react-native';
import { RadioButton } from 'react-native-material-ui';
import ActionSheet from 'react-native-actionsheet';
import DateTime from "react-native-modal-datetime-picker";
import Moment from 'moment';

// API
import { API } from '../../util/api';

// Globals
import * as globals from '../../util/globals';

// Style
const styles = require('../../../AppStyles');

export default class CreateTripIntercantonal extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      spinner: false,
      carId: 0,
      carIdAll: 0,
      carName: "Escoge el auto para viajar",
      carsListName: [],
      seatsAvailable: 0,
      seatsAvailableEnd: 0,
      provincesId: 0,
      provincesIdAll: 0,
      provincesListName: "Escoge Provincia",
      actionProvincesList: [],
      cityIdAll: 0,
      cityNameOrigin: "Escoge Ciudad Origen",
      cityNameDetination: "Escoge Ciudad Destino",
      actionCityList: [],
      parishesIdAll: 0,
      parishesIdOrigin: 0,
      parishesIdDetination: 0,
      actionParishesList: [],
      parishesNameOrigin: "Escoge Parroquia Origen",
      parishesNameDestination: "Escoger Parroquia Destino",
      isDateTimePickerVisible: false,
      departureDate: "Escoge fecha de salida" || "AAAA-MM-DD",
      isTimePickerVisible: false,      
      departureTime: "Escoge hora de salida" || "h:mm a",
      checkedActiveYes: true,
      checkedActiveNo: false,
      carries_parcels: true,
      travelTime: 0,
      pricePerSeat: 0
    };
  }

  componentDidMount() {
    API.listProvincesDriver(this.listProvincesDriverResponse,true)
    API.getCarsDriver(this.getCarsDriverResponse,{},true)
  }

  listProvincesDriverResponse = {
    success: (response) => {
      try {
        let provincesId = response.info.data[0].id
        let provincesIdAll = response.info.data.map((pid)=>pid.id)
        let actionProvincesList = response.info.data.map((nameProvincesList) => nameProvincesList.attributes.name)
        actionProvincesList.push('Cancelar')
        this.setState({
          provincesId:provincesId,
          provincesIdAll:provincesIdAll,
          actionProvincesList:actionProvincesList,
        })
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK'}]);
    }
  }

  getCarsDriverResponse = {
    success: (response) => {
      try {
        if(response.info.approved.data.length == 0){
          Alert.alert(globals.APP_NAME,"No tienes autos Aprovados. Debe esperar su aprovación o registra uno",[{text: 'OK', onPress: () => this.props.props.navigation.navigate("DriverDashboard")}])
        }else{
          let carIdAll = response.info.approved.data.map((cl) => cl.id)
          let carsListName = response.info.approved.data.map((cl) => cl.attributes.brand + " " + cl.attributes.model)
          carsListName.push('Cancelar')
          this.setState({carIdAll:carIdAll, carsListName:carsListName, seatsAvailable:response.info.approved.data[0].attributes.available_seats})
        }
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK'}]);
    }
  }

  _onOpenActionSheetProvinces = () => {
    this.ActionSheetProvinces.show();
  }

  onOpenActionSheetProvinces = (index) => {
    if(this.state.actionProvincesList.length -1 != index){
      let provincesId = this.state.provincesIdAll[index]
      let provincesListName = this.state.actionProvincesList[index]
      this.setState({provincesId:provincesId,provincesListName: provincesListName})
      API.listCitiesDriver(this.listCitiesDriverResponse,provincesId,true)
    }
  }

  listCitiesDriverResponse = {
    success: (response) => {
      try {
        let cityListProvinces = response.info.data.map((cn)=>cn.attributes.name)
        cityListProvinces.push('Cancelar')
        this.setState(
          {
            cityIdAll:response.info.data.map((rid) => rid.id),
            cityNameOrigin:"Escoge Ciudad Origen", // response.info.data[0].attributes.name,
            cityNameDetination:"Escoge Ciudad Destino", // response.info.data[0].attributes.name,
            actionCityList:cityListProvinces,
            parishesNameOrigin: "Escoger Parroquia Origen",
            actionParishesList: [],
          }
        )
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK'}]);
    }
  }

  _onOpenActionSheetCity = (param) => {
    if(param == "origin"){
      this.ActionSheetCityOrigin.show();
    }
    if(param == "destination") {
      this.ActionSheetCityDestination.show();
    }
  }

  onOpenActionSheetCityOrigin = (index) => {
    if(this.state.actionCityList.length -1 != index){
      let cityId = this.state.cityIdAll[index]
      let cityListNameSelect = this.state.actionCityList[index]
      this.setState({cityNameOrigin:cityListNameSelect})
      API.listParishesDriver(this.listParishesDriverResponse,this.state.provincesId,cityId,true)
    }
  }

  onOpenActionSheetCityDestination = (index) => {
    if(this.state.actionCityList.length -1 != index){
      let cityId = this.state.cityIdAll[index]
      let cityListNameSelect = this.state.actionCityList[index]
      this.setState({cityNameDetination:cityListNameSelect})
      API.listParishesDriver(this.listParishesDriverResponse,this.state.provincesId,cityId,true)
    }
  }

  listParishesDriverResponse = {
    success: (response) => {
      try {
        if(response.info.data.length > 0){
          let parishesIdAll = response.info.data.map((pid)=>pid.id)
          let actionParishesList = response.info.data.map((pm)=>pm.attributes.name)
          actionParishesList.push('Cancelar')
          this.setState({parishesIdAll:parishesIdAll, actionParishesList:actionParishesList})
        }else{
          Alert.alert(globals.APP_NAME,"Provincia sin parroquias, puedes escoger otra",[{
            text:'OK',
            onPress: () => {
              this.setState({parishesNameOrigin:"Escoger Parroquia Origen", parishesNameDestination:"Escoger Parroquia Destino", actionParishesList:[]})
              API.listCitiesDriver(this.listCitiesDriverResponse,this.state.provincesId,true)
            }
          }]);
        }
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Error',err.message,[{text:'OK'}]);
    }
  }

  _onOpenActionSheetParishes = (param) => {
    if(param == "origin"){
      this.ActionSheetParishesOrigin.show();
    }
    if(param == "destination") {
      this.ActionSheetParishesDestination.show();
    }
  }

  onOpenActionSheetParishesOrigin = (index) => {
    if(this.state.actionParishesList.length -1 != index){
      let parishesIdSelect = this.state.parishesIdAll[index]
      let parishesListSelect = this.state.actionParishesList[index]
      this.setState({parishesIdOrigin:parishesIdSelect, parishesNameOrigin:parishesListSelect})
    }
  }

  onOpenActionSheetParishesDestination = (itemIndex) => {
    if(this.state.actionParishesList.length -1 != itemIndex){
      let parishesIdSelect = this.state.parishesIdAll[itemIndex]
      let parishesListSelect = this.state.actionParishesList[itemIndex]
      this.setState({parishesIdDetination:parishesIdSelect, parishesNameDestination:parishesListSelect})
    }
  }

  showDatePicker = () => {console.log("Estoy en fecha"), this.setState({isDateTimePickerVisible: true})};

  hideDateTimePicker = () => this.setState({isDateTimePickerVisible: false});

  handleDatePicked = (date) => {
    let datePicked = Moment(new Date(date)).format("YYYY-MM-DD");
    if(datePicked > Moment().format("YYYY-MM-DD")) {
      this.setState({departureDate: datePicked});
      this.hideDateTimePicker();
    }else{
      this.hideDateTimePicker();
      Alert.alert(globals.APP_NAME,'La fecha debe ser mayor a la fecha actual',[{text:'OK', onPress: () => this.showDatePicker()}]);
    }
  }

  showTimePicker = () => {console.log("Estoy en hora"), this.setState({isTimePickerVisible: true})};

  hideTimePicker = () => this.setState({isTimePickerVisible: false});

  handleTimePicked = (time) => {
    let datePicked = Moment(new Date(time)).format("h:mm a");
    this.setState({departureTime: datePicked});
    this.hideTimePicker();
  }

  _onOpenActionSheetCarSelect = () => {
    this.ActionSheetCar.show();
  }

  onOpenActionSheetCarSelect = (itemIndex) => {
    if(this.state.carsListName.length -1 != itemIndex){
      let carId = this.state.carIdAll[itemIndex]
      let carNameSelect = this.state.carsListName[itemIndex]
      this.setState({carId:carId, carName:carNameSelect})
    }
  }

  handleOnPress(checked, value) {
    if(value == true) {
      this.setState({carries_parcels:true,checkedActiveYes:true,checkedActiveNo:false})
    }else{
      this.setState({carries_parcels:false,checkedActiveYes:false,checkedActiveNo:true})
    }
  }

  validateSeatsAvailable(value) {
    if(this.state.seatsAvailable < value.seatsAvailable) {
      Alert.alert('Poolify','No puedes llevar mas de ' + this.state.seatsAvailable + ' puestos en tu auto agregado',[{text:'OK', onPress: () => this.textInput.clear() }]);
    }else{
      this.setState({seatsAvailableEnd:value.seatsAvailable})
    }
  }

  validateTravelTime(value) {
    if(value.travelTime > 72) {
      Alert.alert(globals.APP_NAME,'Horas de viajes debe ser menor a ' + value.travelTime ,[{text:'OK', onPress: () => this.timeTrip.clear() }]);
    }else{
      this.setState({travelTime:value.travelTime})
    }
  }

  validatePricePerSeat(value) {
    if(value.pricePerSeat > 100) {
      Alert.alert(globals.APP_NAME,'Precio de viajes debe ser menor a ' + value.pricePerSeat ,[{text:'OK', onPress: () => this.pricePerSeatInput.clear() }]);
    }else{
      this.setState({pricePerSeat:value.pricePerSeat})
    }
  }

  onPressHandle = () =>{
    if(this.state.provincesListName == "Escoge Provincia") {
      Alert.alert(globals.APP_NAME,'Seleccione provincia',[{text:'OK'}]);
    }else if(this.state.cityNameOrigin == "Escoge Ciudad Origen") {
      Alert.alert(globals.APP_NAME,'Seleccione Ciudad Origen',[{text:'OK'}]);
    }else if (this.state.parishesIdOrigin == 0) {
      Alert.alert(globals.APP_NAME,'Seleccione Parroquia Origen',[{text:'OK'}]);
    }else if(this.state.cityNameDetination == "Escoge Ciudad Destino") {
      Alert.alert(globals.APP_NAME,'Seleccione Ciudad Destino',[{text:'OK'}]);
    }else if(this.state.parishesIdDetination == 0) {
      Alert.alert(globals.APP_NAME,'Seleccione Parroquia Destino',[{text:'OK'}]);
    }else if(this.state.departureDate == "Escoge fecha de salida") {
      Alert.alert(globals.APP_NAME,'Seleccione fecha de salida',[{text:'OK'}]);
    }else if(this.state.departureTime == "Escoge hora de salida"){
      Alert.alert(globals.APP_NAME,'Seleccione hora de salida',[{text:'OK'}]);
    }else if(this.state.carId == 0) {
      Alert.alert(globals.APP_NAME,'Seleccione un auto',[{text:'OK'}]);
    }else if(this.state.seatsAvailableEnd == 0) {
      Alert.alert(globals.APP_NAME,'Coloque cantidad de puestos disponibles para su viaje, no puede estar vacio',[{text:'OK'}]);
    }else if(this.state.travelTime == 0) {
      Alert.alert(globals.APP_NAME,'Coloque tiempo aproximado de viaje, no puede estar vacio',[{text:'OK'}]);
    }else if(this.state.pricePerSeat == 0) {
      Alert.alert(globals.APP_NAME,'Coloque precio del viaje por cada puesto, no puede estar vacio',[{text:'OK'}]);
    }else{
      let dateTime = this.state.departureDate + " " + this.state.departureTime
      let data = {
        "trip": {
          "departure_time": dateTime,
          "origin_id": this.state.parishesIdOrigin,
          "destination_id": this.state.parishesIdDetination,
          "carries_parcels": this.state.carries_parcels,
          "seats_available": this.state.seatsAvailableEnd,
          "travel_time": this.state.travelTime,
          "car_id": this.state.carId,
          "price_per_seat": this.state.pricePerSeat
        }
      }
      this.setState({ spinner: true });
      API.driverCreateTrips(this.driverCreateTripsResponse,data,true);
    }
  }

  driverCreateTripsResponse = {
    success: (response) => {
      try {
        this.setState({spinner: false})
        Alert.alert(globals.APP_NAME,response.message,[{text: 'OK', onPress: () => {
          const {setTripNew} = this.props.props.route.params
          setTripNew(response)
          this.props.props.navigation.navigate("DriverDashboard")
        }}])
      }
      catch (error) {
        this.setState({spinner: false})
        Alert.alert(globals.APP_NAME,error.message,[{text:'OK', onPress: () => this.props.props.navigation.navigate("DriverDashboard")}]);
      }
    },
    error: (err) => {
      this.setState({spinner: false})
      Alert.alert('Err',err.message,[{text:'OK', onPress: () => this.props.props.navigation.navigate("DriverDashboard")}]);
    }
  }

  setFocus = (textField) =>{
    this[textField].focus()
  }
  
  render(){
    return (
      <View style={[styles.containerDashboard,{marginTop:0,backgroundColor:'transparent',marginHorizontal:10}]}>
        <ScrollView bounces={false}>
          <TouchableOpacity style={{marginHorizontal:5,marginVertical:10}} onPress={this._onOpenActionSheetProvinces}>
            <Text>{this.state.provincesListName}</Text>
          </TouchableOpacity>
          <View style={[styles.fixToOptionDouble,{marginHorizontal:5,marginRight:15}]}>
            {this.state.actionCityList.length > 0 ? (
              <TouchableOpacity onPress={()=> this._onOpenActionSheetCity("origin")}>
                <Text>{this.state.cityNameOrigin}</Text>
              </TouchableOpacity>
            ) :null}
            {this.state.actionCityList.length > 0 ? (
              <TouchableOpacity onPress={()=> this._onOpenActionSheetCity("destination")}>
                <Text>{this.state.cityNameDetination}</Text>
              </TouchableOpacity>
            ) :null}
          </View>
          <View style={[styles.fixToOptionDouble,{marginHorizontal:5,marginRight:15}]}>
            {this.state.actionParishesList.length > 0 && this.state.cityNameOrigin != "Escoge Ciudad Origen" ? (
              <TouchableOpacity onPress={()=> this._onOpenActionSheetParishes("origin")}>
                <Text>{this.state.parishesNameOrigin}</Text>
              </TouchableOpacity>
            ) : null}
            {this.state.actionParishesList.length > 0 && this.state.cityNameDetination != "Escoge Ciudad Destino" ? (
              <TouchableOpacity onPress={()=> this._onOpenActionSheetParishes("destination")}>
                <Text>{this.state.parishesNameDestination}</Text>
              </TouchableOpacity>
            ) :null}
          </View>
          <View style={[styles.fixToOptionDouble,{marginHorizontal:5,marginRight:15}]}>
            {this.state.parishesNameDestination != "Escoger Parroquia Destino" ? (
              <View>
                <TouchableOpacity onPress={this.showDatePicker}>
                  <Text >{this.state.departureDate}</Text>
                </TouchableOpacity>
                <DateTime
                  isVisible={this.state.isDateTimePickerVisible}
                  onConfirm={this.handleDatePicked}
                  onCancel={this.hideDateTimePicker}
                  mode='date'
                />
              </View>
            ) :null}
            {this.state.departureDate != "Escoge fecha de salida" ? (
              <View>
                <TouchableOpacity onPress={this.showTimePicker}>
                  <Text>{this.state.departureTime}</Text>
                </TouchableOpacity>
                <DateTime
                  isVisible={this.state.isTimePickerVisible}
                  onConfirm={this.handleTimePicked}
                  onCancel={this.hideTimePicker}
                  mode='time'
                />
              </View>
            ):null }
          </View>
          <View style={[styles.fixToOptionDouble,{marginHorizontal:5,marginRight:15}]}>
            {this.state.departureTime != "Escoge hora de salida" ? (
              <TouchableOpacity onPress={this._onOpenActionSheetCarSelect}>
                <Text>{this.state.carName}</Text>
              </TouchableOpacity>
            ) :null}
          </View>
          {this.state.carName != "Escoge el auto para viajar" ? (
            <View style={{marginHorizontal:5,marginRight:15,marginVertical:10}}>
              <Text>Quieres llevar paquetes?</Text>
              <View style={[styles.fixToOptionDouble,{marginVertical:0}]}>
                <RadioButton
                  label="SI"
                  checked={this.state.checkedActiveYes}
                  value="true"
                  onCheck={(checked, value) => this.handleOnPress(checked, value)}
                />
                <RadioButton
                  label="NO"
                  checked={this.state.checkedActiveNo}
                  value="false"
                  onCheck={(checked, value) => this.handleOnPress(checked, value)}
                />
              </View>
            </View>
          ) :null}
          {this.state.carName != "Escoge el auto para viajar" ? (
            <View>
              <View style={[styles.fixToOptionDouble,{marginHorizontal:5,marginRight:15}]}>
                <TextInput
                  ref={input => { this.textInput = input }}
                  style={[styles.input,{marginHorizontal:0,width:150}]}
                  onChangeText={(seatsAvailable) => this.validateSeatsAvailable({ seatsAvailable })}
                  value={this.state.seatsAvailable}
                  placeholder="Cant. de asientos"
                  keyboardType="number-pad"
                  returnKeyType={"next"}
                  onSubmitEditing={() => this.setFocus("timeTrip")}
                />
                <TextInput
                  ref={ref => (this.timeTrip = ref)}
                  style={[styles.input,{marginHorizontal:0,width:150}]}
                  onChangeText={(travelTime) => this.validateTravelTime({travelTime})}
                  value={this.state.travelTime}
                  placeholder="Horas de viaje"
                  keyboardType="number-pad"
                  returnKeyType={"next"}
                  onSubmitEditing={() => this.setFocus("pricePerSeatInput")}
                />
              </View>
              <TextInput
                ref={ref => (this.pricePerSeatInput = ref)}
                style={[styles.input,{marginHorizontal:5,marginRight:15}]}
                onChangeText={(pricePerSeat) => this.validatePricePerSeat({pricePerSeat})}
                value={this.state.pricePerSeat}
                placeholder="Precio por puesto"
                keyboardType="number-pad"
                returnKeyType={"done"}
                onSubmitEditing={() => Keyboard.dismiss()}
              />
            </View>
          ) :null}
        </ScrollView>
        {this.state.spinner == true ? (
          <View style={[styles.loginButton,{marginVertical:10}]}>
            <ActivityIndicator size="small" color="#fff" />
          </View>
        ):(
          <TouchableOpacity style={[styles.loginButton,{marginVertical:10}]} onPress={() => this.onPressHandle()}>
            <Text style={styles.textOptionButtonReverse}>Crear</Text>
          </TouchableOpacity>
        )}
        <ActionSheet
          ref={o => this.ActionSheetProvinces = o}
          title={'Escoge Provincia'}
          options={this.state.actionProvincesList}          
          cancelButtonIndex={this.state.actionProvincesList.length -1}
          tintColor={'#69b58a'}
          onPress={(index) => { this.onOpenActionSheetProvinces(index) }}
        />
        <ActionSheet
          ref={o => this.ActionSheetCityOrigin = o}
          title={'Escoge Ciudad Origen'}
          options={this.state.actionCityList}
          cancelButtonIndex={this.state.actionCityList.length -1}
          tintColor={'#69b58a'}
          onPress={(index) => { this.onOpenActionSheetCityOrigin(index) }}
        />
        <ActionSheet
          ref={o => this.ActionSheetCityDestination = o}
          title={'Escoge Ciudad Destino'}
          options={this.state.actionCityList}
          cancelButtonIndex={this.state.actionCityList.length -1}
          tintColor={'#69b58a'}
          onPress={(index) => { this.onOpenActionSheetCityDestination(index) }}
        />
        <ActionSheet
          ref={o => this.ActionSheetParishesOrigin = o}
          title={'Escoge Parroquia Origen'}
          options={this.state.actionParishesList}          
          cancelButtonIndex={this.state.actionParishesList.length -1}
          tintColor={'#69b58a'}
          onPress={(index) => { this.onOpenActionSheetParishesOrigin(index) }}
        />
        <ActionSheet
          ref={o => this.ActionSheetParishesDestination = o}
          title={'Escoge Parroquia Destino'}
          options={this.state.actionParishesList}          
          cancelButtonIndex={this.state.actionParishesList.length -1}
          tintColor={'#69b58a'}
          onPress={(index) => { this.onOpenActionSheetParishesDestination(index) }}
        />
        <ActionSheet
          ref={o => this.ActionSheetCar = o}
          title={'Escoge tú carro'}
          options={this.state.carsListName}          
          cancelButtonIndex={this.state.carsListName.length -1}
          tintColor={'#69b58a'}
          onPress={(index) => { this.onOpenActionSheetCarSelect(index) }}
        />
      </View>
    );
  }
}