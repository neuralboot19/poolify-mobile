import React from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import { Card } from 'react-native-material-ui';
import Moment from 'moment';

// API
import { API } from '../../util/api';

// Style
const styles = require('../../../AppStyles');

var _updated = false;

export default class TripListDriver extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      isOnRefresh: false,
      tripList: [],
    };
  }

  componentDidMount(){
    _updated = false;
    API.listTripsDriver(this.listTripsDriverResponse,{},true);
  }

  componentDidUpdate(){
    if (_updated) {
      API.listTripsDriver(this.listTripsDriverResponse,{},true);
      _updated = false;
    }
  }
  

  onRefresh = () =>{
    this.setState({ tripList: [], isOnRefresh: true })
    API.listTripsDriver(this.listTripsDriverResponse,{},true);
    _updated = false;
  }

  listTripsDriverResponse = {
    success: (response) => {
      try {
        let newTripData = response.info.open_trips.data
        this.setState({
          tripList: newTripData,
          isOnRefresh: false,
        })
        _updated = true;
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK'}]);
    }
  }

  ListEmptyComponent = () =>{
    return(
      <View style={{marginHorizontal:32,marginVertical:10}} >
        <Text style={styles.subTitle}>No tienes viajes, crea tu viaje y aumenta tus ingresos.</Text>
      </View>
    )
  }

  renderItem = (item) =>{
    let data = item.item
    let carriesParcels = null
    let departureTime = Moment.utc(data.attributes.departure_time).format('DD/MM/YYYY h:mm a')
    if (data.attributes.carries_parcels){
      carriesParcels = "SI"
    }else{
      carriesParcels = "NO"
    }
    return (
      <Card>
        <View style={[styles.labelButton,{marginHorizontal:0,backgroundColor:"#69b58a"}]}>
          <Text style={[styles.textOptionButtonReverse, {marginHorizontal:23}]}>Viaje Abierto</Text>
        </View>
        <TouchableOpacity onPress={() => this.props.props.navigation.navigate("DriverDetailsTrip",{tripData:data, setTripAll:this.props.setTripAll})}>
          <View style={styles.fixToOptionDouble}>
            <View style={{marginHorizontal:10}}>
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Fecha y hora de salida:</Text></Text>
              <Text style={styles.cardSubTitle}>{departureTime}</Text>
              <Text style={styles.cardSubTitle} numberOfLines={1}><Text style={styles.cardText}>Lleva paquetes: </Text>{carriesParcels}</Text>
              <Text style={styles.cardSubTitle} numberOfLines={1}><Text style={styles.cardText}>Puestos: </Text>{data.attributes.seats_available}</Text>
            </View>
            <View style={styles.containerOptionCard}>
              <Text style={[styles.cardSubTitle,{fontSize:16}]}><Text style={styles.cardText}>Salgo de: </Text>{data.attributes.origin.name}</Text>
              <Text style={[styles.cardSubTitle,{fontSize:16}]}><Text style={styles.cardText}>Voy a: </Text>{data.attributes.destination.name}</Text>
              <Text style={styles.header}>{"$" + data.attributes.price_per_seat.toFixed(2)}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </Card>
    )
  }
  
  render(){
    return (
      <FlatList 
        data = {this.state.tripList}
        renderItem = {this.renderItem}
        refreshing={this.state.isOnRefresh}
        onRefresh={this.onRefresh}
        keyExtractor={(item)=>item.id.toString()}
        ListEmptyComponent={this.ListEmptyComponent}
      />
    );
  }
}