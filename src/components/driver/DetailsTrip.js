import React from 'react';
import { View, Text, TextInput, TouchableOpacity, ScrollView, FlatList, KeyboardAvoidingView, Alert } from 'react-native';
import { Icon, Divider, Card } from 'react-native-material-ui';
import Collapsible from 'react-native-collapsible';
import Moment from 'moment';

// API
import { API } from '../../util/api';

// Globals
import * as globals from '../../util/globals';

// Style
const styles = require('../../../AppStyles');

export default class DriverDetailsTrip extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      idTrip: null,
      dataTrip: {},
      acceptedProposals: [],
      openProposals: [],
      text: "",
      isCallapseOpen: false,
      isCallapseOpen1: true,
      isCallapseOpen2: true,
      isCallapseOpen3: true,
      message: "",
      dateMessages: [],
      sendTextComment: ""
    };
  }

  componentDidMount(){
    API.listTripDriver(this.listTripDriverResponse,this.props.route.params.tripData.id,true)
    API.driverMenssages(this.driverMenssagesResponse,this.props.route.params.tripData.id,true);
  }

  listTripDriverResponse = {
    success: (response) => {
      try {
        this.setState({
          idTrip: response.info.data.id,
          dataTrip: response.info.data.attributes,
          openProposals: response.info.data.attributes.open_proposals,
          acceptedProposals: response.info.data.attributes.accepted_proposals
        })
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK'}]);
    }
  }

  driverMenssagesResponse = {
    success: (response) => {
      try {
        this.setState({dateMessages:response.info.data})
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK'}]);
    }
  }

  renderItem = (item) =>{
    let data = item.item
    return (
      <View style={{marginHorizontal: 40,marginVertical:5}}>
        <Text style={[styles.cardSubTitle,{fontWeight:"800" ,fontSize:18, marginLeft:0}]}>
          <Text style={styles.cardText}>{data.attributes.owner.type == "Customer" ? "Usuario" : "Conductor"} </Text>{data.attributes.owner.full_name}
        </Text>
        <Text style={styles.descriptionText}>{data.attributes.message}</Text>
      </View>
    )
  }

  renderItemOpenPrposals = (item) =>{
    let data = item.item
    return (
      <Card>
        <View style={[styles.fixToOptionDouble,{margin:10}]}>
          <View>
            <Text style={styles.cardSubTitle}>{data.customer.name}</Text>
            <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Nota: </Text>{data.notes}</Text>
            <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Puestos: </Text>{data.seats}</Text>
          </View>
          {this.state.dataTrip.status != "in_progress" && this.state.dataTrip.status != "finished" && this.state.dataTrip.status != "cancelled" ? (
            <TouchableOpacity style={styles.containerOptionCard} onPress={() => this.acceptProposal(data.proposal_id)} >
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>ACEPTAR</Text></Text>
            </TouchableOpacity>
          ) : (<View></View>)}
        </View>
      </Card>
    )
  }

  renderItemAcceptePrposals = (item) =>{
    let data = item.item
    return (
      <Card>
        <View style={[styles.fixToOptionDouble,{margin:10}]}>
          <View>
            <Text style={styles.cardSubTitle}>{data.customer.name}</Text>
            <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Nota: </Text>{data.notes}</Text>
            <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Puestos: </Text>{data.seats}</Text>
          </View>
          {this.state.dataTrip.status != "in_progress" && this.state.dataTrip.status != "finished" && this.state.dataTrip.status != "cancelled" ? (
            <TouchableOpacity style={styles.containerOptionCard} onPress={() => this.unacceptProposal(data.proposal_id)} >
              <Text style={styles.cardSubTitle}><Text style={[styles.cardText,{color:'red'}]}>RECHAZAR</Text></Text>
            </TouchableOpacity>
          ) : (<View></View>)}
        </View>
      </Card>
    )
  }

  acceptProposal(id){
    API.driverAcceptProposal(this.driverAcceptProposalResponse,this.props.route.params.tripData.id,id,true);
  }

  unacceptProposal(id){
    API.driverUnacceptProposal(this.driverUnacceptProposalResponse,this.props.route.params.tripData.id,id,true);
  }

  driverAcceptProposalResponse = {
    success: (response) => {
      try {
        Alert.alert(globals.APP_NAME,response.message,[{text:'OK', onPress: () => API.listTripDriver(this.listTripDriverResponse,this.props.route.params.tripData.id,true) }]);
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK'}]);
    }
  }

  driverUnacceptProposalResponse = {
    success: (response) => {
      try {
        Alert.alert(globals.APP_NAME,response.message,[{text:'OK', onPress: () => API.listTripDriver(this.listTripDriverResponse,this.props.route.params.tripData.id,true) }]);
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK'}]);
    }
  }

  ItemSeparatorComponent = () =>{
    return(
      <View style={{marginHorizontal: 40}}><Divider/></View>
    )
  }

  ListEmptyComponent = () =>{
    return(
      <View style={{flex:1,alignItems:'center',justifyContent:'center',scaleY:-1}} >
        <Text>{this.state.text}</Text>
      </View>
    )
  }

  sendComment = () =>{
    if(this.state.sendTextComment == ""){
      Alert.alert(globals.APP_NAME,'It cant be empty',[{text:'OK'}]);
    }else{
      let data = {
        "message": {
          "message": this.state.sendTextComment
        }
      }
      API.driverCreatedMessages(this.driverCreatedMessagesResponse,data,this.props.route.params.tripData.id,true);
    }
  }

  driverCreatedMessagesResponse = {
    success: (response) => {
      try {
        Alert.alert(globals.APP_NAME,response.message,[{text:'OK', onPress: () => this.updateResponseMessages(response) }]);
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK'}]);
    }
  }

  updateResponseMessages = (response) => {
    this.note.clear()
    let newDate = this.state.dateMessages.map((d)=>d)
    newDate.push(response.info.data)
    this.setState({dateMessages: newDate})
  }

  deleteTripTap(){
    Alert.alert(globals.APP_NAME,"Esta seguro de cancelar este viaje?",[{text: 'NO'},{text: 'SI', onPress: () => API.driverCancelTrip(this.driverCancelTripResponse,this.props.route.params.tripData.id,true)}])
  }

  driverCancelTripResponse = {
    success: (response) => {
      try {
        Alert.alert(globals.APP_NAME,response.message,[{text: 'OK', onPress: () => {
          this.props.navigation.goBack()
        }}])
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK'}]);
    }
  }

  finishedTrip(){
    API.driverfinishedTrip(this.driverfinishedTripResponse,this.props.route.params.tripData.id,true);
  }

  driverfinishedTripResponse = {
    success: (response) => {
      try {
        Alert.alert(globals.APP_NAME,response.message,[{text: 'OK', onPress: () => {
          const {setTripFinished} = this.props.route.params
          setTripFinished()
          this.props.navigation.goBack()
        }}])
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK'}]);
    }
  }

  startTrip(){
    API.driverStartTrip(this.driverStartTripResponse,this.props.route.params.tripData.id,true);
  }

  driverStartTripResponse = {
    success: (response) => {
      try {
        Alert.alert(globals.APP_NAME,response.message,[{text: 'OK', onPress: () => {
          const {setTripAll} = this.props.route.params
          setTripAll()
          this.props.navigation.goBack()
        }}])
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK'}]);
    }
  }

  render(){
    let departure_time = Moment(new Date(this.state.dataTrip.departure_time)).utcOffset(-5).format('DD/MM/YYYY h:mm a')
    let nameOrigin = ""
    let nameDestination = ""
    if(this.state.dataTrip.origin != undefined) {
      nameOrigin = this.state.dataTrip.origin.name
    }
    if(this.state.dataTrip.destination != undefined) {
      nameDestination = this.state.dataTrip.destination.name
    }
    return (
      <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between',marginTop:80}}>
        <View style={{flex: 1}}>
          <View style={[styles.fixToOptionDouble,{marginHorizontal:10}]}>
            <TouchableOpacity onPress={() => this.setState({isCallapseOpen: !this.state.isCallapseOpen, isCallapseOpen1: true, isCallapseOpen2: true, isCallapseOpen3: true})}>
              <View style={[styles.optionButton,{justifyContent:'space-between',backgroundColor: "#69b58a",width:160,marginHorizontal:2}]}>
                <Text style={[styles.textOptionButtonReverse,{marginHorizontal:10}]}>Detalles</Text>
                <Icon style={{color:'#fff'}} name={(this.state.isCallapseOpen) ? "arrow-drop-down" : "arrow-drop-up"}/>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress= {() => this.setState({isCallapseOpen: true, isCallapseOpen1: !this.state.isCallapseOpen1, isCallapseOpen2: true, isCallapseOpen3: true, text: "Sin Propuestas"})}>
              <View style={[styles.optionButton,{justifyContent:'space-between',backgroundColor: "#69b58a",width:160,marginHorizontal:2}]}>
                <Text style={[styles.textOptionButtonReverse,{marginHorizontal:10}]}>Interesados</Text>
                <Icon style={{color:'#fff'}} name={(this.state.isCallapseOpen1) ? "arrow-drop-down" : "arrow-drop-up"}/>
              </View>
            </TouchableOpacity>
          </View>
          <Collapsible collapsed={this.state.isCallapseOpen} duration={200}>
            <View style={[styles.fixToOptionDouble,{marginHorizontal: 32, marginVertical: 10}]}>
              <View>
                <Text style={[styles.cardSubTitle,{fontSize:14}]}><Text style={styles.cardText}>Fecha y hora de salida</Text></Text>
                <Text style={[styles.cardSubTitle,{fontSize:14}]}><Text style={styles.subText}>{departure_time}</Text></Text>
                <Text style={[styles.cardSubTitle,{fontSize:14,marginTop:10}]}><Text style={styles.cardText}>Destino</Text></Text>
                <Text style={[styles.cardSubTitle,{fontSize:14}]}><Text style={styles.subText}>Voy a: {nameOrigin}</Text></Text>
                <Text style={[styles.cardSubTitle,{fontSize:14}]}><Text style={styles.subText}>Llego a: {nameDestination}</Text></Text>
                <Text style={[styles.cardSubTitle,{fontSize:14,marginTop:10}]}><Text style={styles.cardText}>Tiempo estimo de llegada</Text></Text>
                <Text style={[styles.cardSubTitle,{fontSize:14}]}><Text style={styles.subText}>Aproximadamente: {this.state.dataTrip.travel_time + "HS"}</Text></Text>
                <Text style={[styles.cardSubTitle,{fontSize:14,marginTop:10}]}><Text style={styles.cardText}>Puestos disponibles </Text>{this.state.dataTrip.seats_available}</Text>
                <Text style={[styles.cardSubTitle,{fontSize:14,marginTop:10}]}><Text style={styles.cardText}>Llevo paquetes </Text>{this.state.dataTrip.carries_parcels ? "SI" : "NO"}</Text>
              </View>
              <View style={styles.containerOptionCard}>
                <Text style={[styles.header,{fontSize:40}]}>{"$ " + this.state.dataTrip.price_per_seat}</Text>
              </View>
            </View>
          </Collapsible>
          <Collapsible collapsed={this.state.isCallapseOpen1} duration={200}>
            <View style={{marginHorizontal: 32, marginVertical: 10}}>
              <FlatList 
                data = {this.state.openProposals}
                renderItem = {this.renderItemOpenPrposals}
                keyExtractor={(item)=>item.toString()}
                ListEmptyComponent={this.ListEmptyComponent}
                inverted
              />
            </View>
          </Collapsible>
          <TouchableOpacity onPress={() => this.setState({isCallapseOpen: true, isCallapseOpen1: true, isCallapseOpen2: true, isCallapseOpen3: !this.state.isCallapseOpen3, text: "No has aceptado pasajeros"})}>
            <View style={[styles.loginButton,{justifyContent:'space-between',margin:10}]}>
              <Text style={[styles.textOptionButtonReverse,{marginHorizontal:10}]}>Pasajeros Aceptados</Text>
              <Icon style={{color:'#fff'}} name={(this.state.isCallapseOpen3) ? "arrow-drop-down" : "arrow-drop-up"}/>
            </View>
          </TouchableOpacity>
          <Collapsible collapsed={this.state.isCallapseOpen3} duration={200}>
            <View style={{marginHorizontal: 32, marginVertical: 10}}>
              <FlatList 
                data = {this.state.acceptedProposals}
                renderItem = {this.renderItemAcceptePrposals}
                keyExtractor={(item)=>item.toString()}
                ListEmptyComponent={this.ListEmptyComponent}
                inverted
              />
            </View>
          </Collapsible>
          {this.state.dataTrip.status != "finished" && this.state.dataTrip.status != "cancelled" ? (
            <TouchableOpacity onPress={() => this.setState({isCallapseOpen: true, isCallapseOpen1: true, isCallapseOpen2: !this.state.isCallapseOpen2, isCallapseOpen3: true, text: "Sin Comentarios"})}>
              <View style={[styles.loginButton,{justifyContent:'space-between',margin:10}]}>
                <Text style={[styles.textOptionButtonReverse,{marginHorizontal:10}]}>Comentarios</Text>
                <Icon style={{color:'#fff'}} name={(this.state.isCallapseOpen2) ? "arrow-drop-down" : "arrow-drop-up"}/>
              </View>
            </TouchableOpacity>
          ) : (<View></View> )}
          <ScrollView bounces={false}>
            <Collapsible collapsed={this.state.isCallapseOpen2} duration={200}>
              <FlatList 
                data = {this.state.dateMessages}
                renderItem = {this.renderItem}
                ItemSeparatorComponent={this.ItemSeparatorComponent}
                keyExtractor={(item)=>item.id.toString()}
                ListEmptyComponent={this.ListEmptyComponent}
                inverted
              />
            </Collapsible>
          </ScrollView>
        </View>
        <View>
        </View>
        {this.state.dataTrip.status == "cancelled" ? (
          <TouchableOpacity style={[styles.loginButton,{backgroundColor:'#F4F5F7',marginVertical:10}]} >
            <Text style={[styles.textOptionButtonReverse,{color:'red'}]}>Viaje Cancelado</Text>
          </TouchableOpacity>
        ) : (
          <View>
            {!this.state.isCallapseOpen2 && !this.state.dateMessages.length == 0 ? (
              <KeyboardAvoidingView behavior="padding">
                <TextInput
                  ref={input => { this.note = input }}
                  style={[styles.input,{height:80}]}
                  onChangeText={note => this.setState({sendTextComment : note})}
                  value={this.state.sendTextComment}
                  placeholder="Responde a tus Usuarios."
                  keyboardType="default"
                  multiline={true}
                  maxLength={150}
                  returnKeyType={"done"}
                  onSubmitEditing={() => Keyboard.dismiss()}
                />
                <TouchableOpacity style={[styles.loginButton,{marginVertical:10}]} onPress={() => this.sendComment()}>
                  <Text style={styles.textOptionButtonReverse}>Enviar</Text>
                </TouchableOpacity>
              </KeyboardAvoidingView>
            ) : (
              <View style={{marginHorizontal:10}}>
                {this.state.dataTrip.status == "open" ? (
                  <TouchableOpacity style={[styles.loginButton,{backgroundColor:'#F4F5F7'}]} onPress={() => this.deleteTripTap()}>
                    <Text style={[styles.textOptionButtonReverse,{color:'red'}]}>Cancelar viaje</Text>
                  </TouchableOpacity>
                ) : null}
                {this.state.dataTrip.status == "finished" ? (
                  <TouchableOpacity style={[styles.loginButton,{backgroundColor:'#F4F5F7',marginVertical:10}]} >
                    <Text style={[styles.textOptionButtonReverse,{color:'red'}]}>Viaje Finalizado</Text>
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity style={[styles.loginButton,{marginVertical:10}]} onPress={() => this.state.dataTrip.status == "in_progress" ? this.finishedTrip() : this.startTrip()}>
                    <Text style={styles.textOptionButtonReverse}>{this.state.dataTrip.status == "in_progress" ? "Finalizar Viaje" : "Iniciar viaje"}</Text>
                  </TouchableOpacity>
                )}
              </View>
            )}
          </View>
        ) }
      </View>
    );
  }
}