import React from 'react';
import { View } from 'react-native';
import { RadioButton } from 'react-native-material-ui';

// Component
import CreateTripIntercantonal from './CreateTripIntercantonal';
import CreateTripInterprovincial from './CreateTripInterprovincial';

// Style
const styles = require('../../../AppStyles');

export default class CreateTrip extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      checkedActiveYes: true,
      checkedActiveNo: false,
      radioGroup: true,
    };
  }

  handleOnPress(checked, value) {
    if(value == true) {
      this.setState({radioGroup:true,checkedActiveYes:true,checkedActiveNo:false})
    }else{
      this.setState({radioGroup:false,checkedActiveYes:false,checkedActiveNo:true})
    }
  }
  
  render(){
    return (
      <View style={[styles.containerDashboard,{marginTop:80,backgroundColor:'transparent',marginHorizontal:10}]}>
        <View style={[styles.fixToOptionDouble,{marginVertical:0}]}>
          <RadioButton
            label="Intercantonal"
            checked={this.state.checkedActiveYes}
            value={true}
            onCheck={(checked, value) => this.handleOnPress(checked, value)}
          />
          <RadioButton
            label="Interprovincial"
            checked={this.state.checkedActiveNo}
            value={false}
            onCheck={(checked, value) => this.handleOnPress(checked, value)}
          />
        </View>
        {this.state.radioGroup ?
          <CreateTripIntercantonal props={this.props} />
        :
          <CreateTripInterprovincial props={this.props} />
        }
      </View>
    );
  }
}