import React, {Component} from 'react';
import {View, Text, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native';
import { Icon, Card } from 'react-native-material-ui';

// API
import { API } from '../../util/api';

// Style
const styles = require('../../../AppStyles');

export default class DiverListCar extends Component {

  constructor(props){
    super(props)
    this.state = {
      carList : [],
      carListApproved : [],
      carListDeclined : []
    }
  }

  componentDidMount(){
    API.getCarsDriver(this.getCarsDriverResponse,{},true)
  }

  getCarsDriverResponse = {
    success: (response) => {
      try {
        let infoStateOnHold = null
        let infoStateApproved = null
        let infoStateDeclined = null
        if(response.info.on_hold.data.length != 0){
          infoStateOnHold = response.info.on_hold.data
        }
        if(response.info.approved.data.length != 0){
          infoStateApproved = response.info.approved.data
        }
        if(response.info.declined.data.length != 0){
          infoStateDeclined = response.info.declined.data
        }
        this.setState({
          carList : infoStateOnHold,
          carListApproved : infoStateApproved,
          carListDeclined : infoStateDeclined
        })
      } catch (error) {
        console.log('getCarsDriverResponse catch error ' + JSON.stringify(error));
      }
    },
    error: (err) => {
      console.log('getCarsDriverResponse error ' + JSON.stringify(err));
    }
  }

  renderItem = (item) =>{
    let data = item.item
    let nameStatus = ""
    let nameIcon = ""
    let color = ""
    if(data.attributes.status == "approved"){
      nameStatus = "Aprobado",
      nameIcon = "check",
      color = "#69b58a"
    } else if (data.attributes.status == "declined") {
      nameStatus = "Rechazado",
      nameIcon = "close",
      color = "red"
    } else if (data.attributes.status == "on_hold") {
      nameStatus = "En espera de aprovación",
      nameIcon = "warning",
      color = "orange"
    }
    return(
      <Card>
        <View style={[styles.fixToOptionDouble,{margin:10}]}>
          <View>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Marca: </Text>{data.attributes.brand}</Text>
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Modelo: </Text>{data.attributes.model}</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Placa: </Text>{data.attributes.licence_plate}</Text>
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Color: </Text>{data.attributes.color}</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.cardSubTitle}><Text style={styles.cardText}>Asientos configurado: </Text>{data.attributes.available_seats}</Text>
            </View>
          </View>
          <View style={{alignItems:'center'}}>
            <Icon color={color} name={nameIcon}/>
            <Text style={[styles.cardSubTitle,{marginHorizontal:10}]}><Text style={[styles.cardText,{color}]}>{nameStatus}</Text></Text>
          </View>
        </View>
      </Card>
    )
  }

  ListEmptyComponent = () =>{
    return(
      <View style={{marginHorizontal:32,marginVertical:10}} >
        <Text style={styles.subTitle}>Sin Autos</Text>
      </View>
    )
  }

  //======================================================================
  // render
  //======================================================================

  render(){
    return(
      <View style={[styles.containerProfile,{marginHorizontal:10}]}>
        {this.state.carList != null ? (
          <FlatList 
            data = {this.state.carList}
            renderItem = {this.renderItem}
            keyExtractor={(item)=>item.id.toString()}
            ListEmptyComponent={this.ListEmptyComponent}
          />
        ):null}
        {this.state.carListApproved != null ? (
          <FlatList 
            data = {this.state.carListApproved}
            renderItem = {this.renderItem}
            keyExtractor={(item)=>item.id.toString()}
            ListEmptyComponent={this.ListEmptyComponent}
          />
        ):null}
        {this.state.carListDeclined != null ? (
          <FlatList 
            data = {this.state.carListDeclined}
            renderItem = {this.renderItem}
            keyExtractor={(item)=>item.id.toString()}
            ListEmptyComponent={this.ListEmptyComponent}
          />
        ):null}
        {this.state.spinner == true ? (
          <View style={[styles.loginButton,{marginVertical:10}]}>
            <ActivityIndicator size="small" color="#fff" />
          </View>
        ):(
          <TouchableOpacity style={[styles.loginButton,{marginVertical:10}]} onPress={this.addCarProfile}>
            <Text style={styles.textOptionButtonReverse}>Agregar Auto</Text>
          </TouchableOpacity>
        )}
      </View>
    )
  }
}
