import React from 'react';
import { View, Text, KeyboardAvoidingView, ScrollView, TextInput, ActivityIndicator, TouchableOpacity, AsyncStorage, Alert, Keyboard } from 'react-native';

// API
import { API } from '../../util/api';

// Globals
import * as globals from '../../util/globals';

// Style
const styles = require('../../../AppStyles');

export default class CustomerUpdatePassword extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      spinner: false,
      currentPassword: "",
      newPassword: "",
      confirmPassword: ""
    };
  }

  updatePassword = () =>{
    if (this.state.currentPassword == '') {
      Alert.alert('Error of validate','Current Password. It cant be empty',[{text:'OK'}]);
    }else if (this.state.newPassword == '') {
      Alert.alert('Error of validate','New Password. It cant be empty',[{text:'OK'}]);
    }else if (this.state.confirmPassword == '') {
      Alert.alert('Error of validate','Confirm Password. It cant be empty',[{text:'OK'}]);
    }else if (this.state.newPassword == this.state.confirmPassword){
      let data = {
        "customer": {
          "current_password": this.state.currentPassword,
          "password": this.state.newPassword,
          "password_confirmation": this.state.confirmPassword
        }
      }
      API.customerUpdatePassword(this.updatePasswordResponse,data,true);
    }else{
      Alert.alert(globals.APP_NAME,"Mismatch new password")
    }
  }

  updatePasswordResponse = {
    success: (response) => {
      try {
        Alert.alert(globals.APP_NAME,response.message,[{text: 'OK', onPress: () => {
          AsyncStorage.clear().then(()=>{
            this.props.navigation.navigate("Home")
          })
        }}])
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      console.log("ERRRRRRR",err)
      Alert.alert('Error of validate','Current Password is incorrect',[{text:'OK'}]);
    }
  }

  setFocus = (textField) =>{
    this[textField].focus()
  }
  
  render(){
    return (
      <KeyboardAvoidingView style={styles.containerProfile} behavior="padding">
        <ScrollView bounces={false}>
          <View style={{marginHorizontal: 30, marginTop:10 }}>
            <Text style={styles.subHeader}>Current password</Text>
          </View>
          <TextInput
            style={styles.input}
            onChangeText={(currentPassword) => this.setState({currentPassword : currentPassword})}
            value={this.state.currentPassword}
            placeholder="Current Password"
            secureTextEntry={true}
            returnKeyType={"next"}
            onSubmitEditing={() => this.setFocus("newPassword")}
          />
          <View style={{marginHorizontal: 30, marginTop:10 }}>
            <Text style={styles.subHeader}>New password</Text>
          </View>
          <TextInput
            ref={ref => (this.newPassword = ref)}
            style={styles.input}
            onChangeText={(newPassword) => this.setState({ newPassword })}
            value={this.state.newPassword}
            placeholder="New Password"
            secureTextEntry={true}
            returnKeyType={"next"}
            onSubmitEditing={() => this.setFocus("confirmPassword")}
          />
          <View style={{marginHorizontal: 30, marginTop:10 }}>
            <Text style={styles.subHeader}>Confirm new password</Text>
          </View>
          <TextInput
            ref={ref => (this.confirmPassword = ref)}
            style={styles.input}
            onChangeText={confirmPassword => this.setState({ confirmPassword })}
            value={this.state.confirmPassword}
            placeholder="Confirm Password"
            secureTextEntry={true}
            returnKeyType={"done"}
            onSubmitEditing={() => Keyboard.dismiss()}
          />
        </ScrollView>
        {this.state.spinner == true ? (
          <View style={{marginVertical:10}}>
            <ActivityIndicator size="small" color="#69b58a" />
          </View>
        ):(
          <TouchableOpacity style={[styles.loginButton,{marginVertical:10}]} onPress={this.updatePassword}>
            <Text style={styles.textOptionButtonReverse}>Update</Text>
          </TouchableOpacity>
        )}
      </KeyboardAvoidingView>
    );
  }
}