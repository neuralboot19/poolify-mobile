import React from 'react';
import { View, Text, TextInput, TouchableOpacity, Alert, ActivityIndicator, Keyboard, ScrollView, AsyncStorage } from 'react-native';
import { RadioButton } from 'react-native-material-ui';

// API
import { API } from '../../util/api';

// Globals
import * as globals from '../../util/globals';

// Component
import TripList from './TripList';

// Style
const styles = require('../../../AppStyles');

export default class DriverDashboardHome extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      spinner: false,
      brand: "",
      model: "",
      licence_plate: "",
      color: "",
      available_seats: 0,
      checkedActiveYes: true,
      checkedActiveNo: false,
      carries_parcels: true,
      image: "",
      carsList: [],
      availableFrom: true,
    };
  }

  componentDidMount(){
    API.getCarsDriver(this.getCarsDriverResponse,{},true)
  }

  getCarsDriverResponse = {
    success: (response) => {
      try {
        if(response.info.on_hold.data.length == 0){
          this.setState({availableFrom:true})
        }else{
          this.setState({carsList:response.info.on_hold,availableFrom:false})
          if (globals.status == "on_hold") {
            Alert.alert(globals.APP_NAME,"Usuario no activo en la plataforma, regrese mas tarde.",[{text: 'OK', onPress: () => this.initStar()}])
          } else if (response.info.approved.data.length > 0 == false) {
            Alert.alert(globals.APP_NAME,"Debe esperar la aprovación de su auto",[{text: 'OK', onPress: () => this.initStar()}])
          }
        }
      } catch (error) {
        Alert.alert('Error',error.message,[{text:'OK'}]);
      }
    },
    error: (err) => {
      Alert.alert('Err',err.message,[{text:'OK'}]);
    }
  }

  initStar = () => {
    AsyncStorage.clear();
    globals.access_token = null
    globals.id = null
    globals.first_name = null
    globals.last_name = null
    globals.email = null
    globals.status = null
    globals.national_id = 0
    globals.cell_phone = 0
    globals.birthday = null
    globals.avatar = null
    globals.reviews_received = null
    globals.open_trips = null
    this.props.props.navigation.navigate('Home')
  }

  handleOnPress(checked, value) {
    if(value == true) {
      this.setState({carries_parcels:true,checkedActiveYes:true,checkedActiveNo:false})
    }else{
      this.setState({carries_parcels:false,checkedActiveYes:false,checkedActiveNo:true})
    }
  }

  saveParamsFormCar = async() =>{
    if (this.state.brand === '') {
      Alert.alert('Validación','Ingrese la marca de su auto',[{text:'OK'}]);
    } else if (this.state.model === '') {
      Alert.alert('Validación','Ingrese el modelo de su auto',[{text:'OK'}]);
    } else if (this.state.licence_plate === '') {
      Alert.alert('Validación','Ingrese su numero de matricula o licencia',[{text:'OK'}]);
    } else if (this.state.color === '') {
      Alert.alert('Validación','Debe colocar el color del auto',[{text:'OK'}]);
    } else if (this.state.available_seats == 0) {
      Alert.alert('Validación','Debe colocar cantidad de puestos disponibles de tu auto',[{text:'OK'}]);
    } else {
      let data = {
        "car": {
          "available_seats": this.state.available_seats,
          "carries_parcels": this.state.carries_parcels,
          "model": this.state.model,
          "brand": this.state.brand,
          "licence_plate": this.state.licence_plate,
          "color": this.state.color,
          "image": this.state.image
        }
      }
      this.setState({ spinner: true });
      API.addCarsDriver(this.addCarsDriverResponse,data,true);
    }
  }

  addCarsDriverResponse = {
    success: (response) => {
      try {
        this.setState({carsList:response.info.data, spinner: false})
        Alert.alert("Poolify",response.message,[{text: 'OK', onPress: () => this.updateForm(false)}])
      } catch (error) {
        this.setState({spinner: false })
      }
    },
    error: (err) => {
      this.setState({ errorMessage: err.message})
    }
  }

  updateForm = (v) =>{
    if((v) == false){
      AsyncStorage.clear();
      globals.access_token = null
      globals.id = null
      globals.first_name = null
      globals.last_name = null
      globals.email = null
      globals.status = null
      globals.national_id = 0
      globals.cell_phone = 0
      globals.birthday = null
      globals.avatar = null
      globals.reviews_received = null
      globals.open_trips = null
      this.setState({availableFrom:false})
      this.props.props.navigation.navigate('Home')
    }else{
      this.setState({availableFrom:true})
    }
  }

  setFocus = (textField) =>{
    this[textField].focus()
  }
  
  render(){
    if (globals.status != "on_hold") {
      return (
        <TripList setTripAll={this.props.setTripAll} props={this.props.props} />
      )
    }else{
      return (
        this.state.availableFrom == true ? (
          <View style={[styles.containerDashboard,{marginTop:0,marginHorizontal:10}]}>
            <View style={styles.circle} />
            <Text style={styles.header}>Para tú aprovación</Text>
            <Text style={styles.subTitle}>Debes colocar los datos de tu auto.</Text>
            <ScrollView bounces={false}>
              <View style={[styles.fixToOptionDouble,{marginTop:20}]}>
                <View>
                  <Text style={styles.subHeader}>Marca</Text>
                  <TextInput
                    style={[styles.input,{marginHorizontal:0,width:160}]}
                    onChangeText={(brand) => this.setState({ brand })}
                    value={this.state.brand}
                    placeholder="Marca"
                    keyboardType="default"
                    returnKeyType={"next"}
                    onSubmitEditing={() => this.setFocus("model")}
                  />
                </View>
                <View>
                  <Text style={styles.subHeader}>Modelo</Text>
                  <TextInput
                    ref={ref => (this.model = ref)}
                    style={[styles.input,{marginHorizontal:0,width:160}]}
                    onChangeText={(model) => this.setState({ model })}
                    value={this.state.model}
                    placeholder="Modelo"
                    keyboardType="default"
                    returnKeyType={"next"}
                    onSubmitEditing={() => this.setFocus("licence_plate")}
                  />
                </View>
              </View>
              <View style={[styles.fixToOptionDouble,{marginTop:0}]}>
                <View>
                  <Text style={styles.subHeader}>Placa</Text>
                  <TextInput
                    ref={ref => (this.licence_plate = ref)}
                    style={[styles.input,{marginHorizontal:0,width:160,textTransform: 'uppercase'}]}
                    onChangeText={(licence_plate) => this.setState({ licence_plate })}
                    value={this.state.licence_plate}
                    placeholder="Placa"
                    autoCapitalize='characters'
                    returnKeyType={"next"}
                    onSubmitEditing={() => this.setFocus("color")}
                  />
                </View>
                <View>
                  <Text style={styles.subHeader}>Color</Text>
                  <TextInput
                    ref={ref => (this.color = ref)}
                    style={[styles.input,{marginHorizontal:0,width:160}]}
                    onChangeText={color => this.setState({ color })}
                    value={this.state.color}
                    placeholder="Color"
                    keyboardType="default"
                    returnKeyType={"next"}
                    onSubmitEditing={() => this.setFocus("available_seats")}
                  />
                </View>
              </View>
              <View style={{marginTop:0}}>
                <Text style={styles.subHeader}>Puestos Disponibles</Text>
              </View>
              <TextInput
                ref={ref => (this.available_seats = ref)}
                style={styles.input}
                onChangeText={available_seats => this.setState({ available_seats })}
                value={this.state.available_seats}
                placeholder="Puestos Disponibles"
                keyboardType="number-pad"
                returnKeyType={"done"}
                onSubmitEditing={() => Keyboard.dismiss()}
              />
              <View style={{marginTop:10 }}>
                <Text style={styles.subHeader}>¿Quieres llevar paquetes?</Text>
              </View>
              <View style={[styles.fixToOptionDouble,{marginTop:0}]}>
                <RadioButton
                  label="SI"
                  checked={this.state.checkedActiveYes}
                  value={true}
                  onCheck={(checked, value) => this.handleOnPress(checked, value)}
                />
                <RadioButton
                  label="NO"
                  checked={this.state.checkedActiveNo}
                  value={false}
                  onCheck={(checked, value) => this.handleOnPress(checked, value)}
                />
              </View>
            </ScrollView>
            {this.state.spinner == true ? (
              <View style={[styles.loginButton,{marginVertical:10}]}>
                <ActivityIndicator size="small" color="#fff" />
              </View>
            ):(
              <TouchableOpacity style={[styles.loginButton,{marginVertical:10}]} onPress={this.saveParamsFormCar}>
                <Text style={styles.textOptionButtonReverse}>Registrar</Text>
              </TouchableOpacity>
            )}
          </View>
        ) : (
          <View style={styles.containerDashboard}>
            <View style={styles.circle} />
            <View style={{marginHorizontal: 32}}>
              <Text style={styles.header}>Hola, {globals.first_name + " " + globals.last_name}</Text>
              <Text style={styles.header}>Gracias por tu registo.</Text>
              <Text style={styles.subTitle}>Estamos evaluando tú aprovación...</Text>
              <Text style={styles.subTitle}>Un asesor(a) se contactará contigo.</Text>
            </View>
          </View>
        )
      )
    }
  }
}
