import React from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';

// Style App
const styles = require('../../AppStyles');

// Components
import LoginCustomer from './customer/Login';
import LoginDriver from './driver/Login';

export default class Home extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      spinner: false,
      loginDriverOption: false
    };
  }

  loginDriver = () => {
    if(this.state.loginDriverOption){
      this.setState({loginDriverOption:false})
    }else{
      this.setState({loginDriverOption:true})
    }
  }

  render(){
    return (
      <View style={styles.containerLogin}>
        <View style={styles.circle} />
        <View></View>
        <View style={{margin: 20}}>
          <Image source={ require('../../assets/Poolify.png')} />
          {this.state.loginDriverOption == false ? (
            <LoginCustomer props={this.props} />
          ) : (
            <LoginDriver props={this.props} />
          )}
        </View>
        <View style={[styles.fixToOptionDouble,{justifyContent:'flex-end',marginHorizontal:32}]}>
          <TouchableOpacity onPress={() => this.loginDriver()}>
            <Text style={styles.textOptionButton}>
              {this.state.loginDriverOption ? "INICIA COMO PASAJERO" : "ENTRA COMO CONDUCTOR"}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}