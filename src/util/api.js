import * as APILIST from './apiURL';
import * as globals from './globals';

export const buildHeader = (headerParams = {}) => {
  let header = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization' : 'Bearer ' + globals.access_token || ''
  }
  Object.assign(header, headerParams);
  return header; 
}

export const API = {

  //Customers API
  loginWithFacebook: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.FACEBOOK_LOGIN, buildHeader());
  },
  signOutCustomer: (onResponse, {}, isHeaderRequired) => {
    request(onResponse, {}, 'DELETE', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.CUSTOMER_SIGNOUT, buildHeader());
  },
  showCustomer: (onResponse, {}, isHeaderRequired) => {
    request(onResponse, {}, 'GET', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.CUSTOMER_SHOW, buildHeader());
  },
  customerUpdateProfile: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'PUT', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.CUSTOMERS_PROFILE, buildHeader());
  },
  listTripsCustomer: (onResponse, isHeaderRequired) => {
    request(onResponse, {}, 'GET', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.CUSTOMER_TRIPS, buildHeader());
  },
  customerPostulateTrips: (onResponse, data, id, isHeaderRequired) => {
    request(onResponse, data ,'POST', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.CUSTOMER_POSTULATE_TRIPS + id + "/proposal", buildHeader());
  },
  listTripCustomer: (onResponse, trip_id, isHeaderRequired) => {
    request(onResponse, {}, 'GET', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.CUSTOMER_TRIPS + "/" + trip_id, buildHeader());
  },
  listMessagesCustomer: (onResponse, trip_id, isHeaderRequired) => {
    request(onResponse, {}, 'GET', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.CUSTOMER_MESSAGES_LIST + trip_id + "/messages", buildHeader());
  },
  listMyTripsCustomer: (onResponse, isHeaderRequired) => {
    request(onResponse, {}, 'GET', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.CUSTOMER_MY_TRIPS, buildHeader());
  },
  createdMessagesCustomer: (onResponse, data, trip_id, isHeaderRequired) => {
    request(onResponse, data ,'POST', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.CUSTOMER_MESSAGES_LIST + trip_id + "/messages", buildHeader());
  },
  customerQuitTrip: (onResponse, id, isHeaderRequired) => {
    request(onResponse, {}, 'DELETE', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.CUSTOMER_TRIPS + "/" + id + "/quit", buildHeader());
  },
  customerSendReview: (onResponse, data, trip_id, isHeaderRequired) => {
    request(onResponse, data, 'POST', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.CUSTOMER_TRIPS + "/" + trip_id + "/review", buildHeader());
  },
  canReview: (onResponse, trip_id, isHeaderRequired) => {
    request(onResponse, {}, 'GET', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.CUSTOMER_TRIPS + "/" + trip_id + "/can_review", buildHeader());
  },
  customerButtonPushed: (onResponse, data, id_trip, isHeaderRequired) => {
    request(onResponse, data, 'POST', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.CUSTOMER_TRIPS + "/" + id_trip + "/panic/pushed", buildHeader());
  },
  customerPushNotification: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'PATCH', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.CUSTOMER_PUSH_NOTIFICATION, buildHeader());
  },

  //Driver API
  loginDriver: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_SINGNIN, buildHeader());
  },
  signOutDriver: (onResponse, {}, isHeaderRequired) => {
    request(onResponse, {}, 'DELETE', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_SIGNOUT, buildHeader());
  },
  driverSignUp: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_SIGNUP, buildHeader());
  },
  getCarsDriver: (onResponse, {} ,isHeaderRequired) => {
    request(onResponse, {}, 'GET', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_CARS, buildHeader());
  },
  addCarsDriver: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_CARS, buildHeader());
  },
  listTripsDriver: (onResponse, {}, isHeaderRequired) => {
    request(onResponse, {}, 'GET', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_TRIPS, buildHeader());
  },
  listProvincesDriver: (onResponse, isHeaderRequired) => {
    request(onResponse, {}, 'GET', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_PROVINCES, buildHeader());
  },
  listCitiesDriver: (onResponse, id, isHeaderRequired) => {
    request(onResponse, {}, 'GET', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_PROVINCES + "/" + id + "/cities", buildHeader());
  },
  listParishesDriver: (onResponse, province_id, city_id, isHeaderRequired) => {
    request(onResponse, {}, 'GET', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_PROVINCES + "/" + province_id + '/cities/' + city_id + '/parishes', buildHeader());
  },
  driverCreateTrips: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_CREATE_TRIPS, buildHeader());
  },
  driverUpdate: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'PUT', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_UPDATE, buildHeader());
  },
  listTripDriver: (onResponse, trip_id, isHeaderRequired) => {
    request(onResponse, {}, 'GET', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_TRIPS + "/" + trip_id, buildHeader());
  },
  driverMenssages: (onResponse, id, isHeaderRequired) => {
    request(onResponse, {}, 'GET', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_TRIPS + "/" + id +'/messages', buildHeader());
  },
  driverCreatedMessages: (onResponse, data, trip_id, isHeaderRequired) => {
    request(onResponse, data ,'POST', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_TRIPS + "/" + trip_id + "/messages", buildHeader());
  },
  driverAcceptProposal: (onResponse, trip_id, proposal_id, isHeaderRequired) => {
    request(onResponse, {}, 'GET', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_TRIPS + "/" + trip_id + '/proposals/' + proposal_id + "/accept", buildHeader());
  },
  driverUnacceptProposal: (onResponse, trip_id, proposal_id, isHeaderRequired) => {
    request(onResponse, {}, 'DELETE', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_TRIPS + "/" + trip_id + '/proposals/' + proposal_id + "/unaccept", buildHeader());
  },
  driverCancelTrip: (onResponse, id, isHeaderRequired) => {
    request(onResponse, {}, 'GET', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_TRIPS + "/" + id + "/cancel", buildHeader());
  },
  driverStartTrip: (onResponse, trip_id, isHeaderRequired) => {
    request(onResponse, {} ,'POST', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_TRIPS + "/" + trip_id + "/start", buildHeader());
  },
  driverfinishedTrip: (onResponse, id, isHeaderRequired) => {
    request(onResponse, {}, 'GET', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_TRIPS + "/" + id + "/finish", buildHeader());
  },
  driverButtonPushed: (onResponse, data, id_trip, isHeaderRequired) => {
    request(onResponse, data, 'POST', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_TRIPS + "/" + id_trip + "/panic/pushed", buildHeader());
  },
  driverPushNotification: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'PATCH', "JSON", isHeaderRequired, APILIST.BASE_URL + APILIST.DRIVER_PUSH_NOTIFICATION, buildHeader());
  },
}

async function request(onResponse, data, type, returnType, isHeaderRequired, featureURL, secureRequest) {
  let response = '';
  console.log("featureURL >>> " + featureURL);
  console.log("secureRequest " + JSON.stringify(secureRequest));
  console.log("data >>> " + JSON.stringify(data));
  console.log("returnType " + returnType);
  console.log("isHeaderRequired " + isHeaderRequired);
  console.log("type " + type);

  try {
    if (type === 'GET') {
      if (isHeaderRequired) {
        response = await fetch(featureURL, {
          method: type,
          headers: secureRequest
        });
      }
      else {
        response = await fetch(featureURL, {
          method: type,
        });
      }
    } else {
      response = await fetch(featureURL, {
        method: type,
        headers: secureRequest,
        body: JSON.stringify(data)
      });
    }
    let responseHEADERS = await response.headers;
    let responseJSON = await response.json();
    if (response.status == 200) {
      onResponse.success(responseJSON, responseHEADERS);
    } else {
      onResponse.error(responseJSON, responseHEADERS);
    }
    if (onResponse.complete) {
      onResponse.complete();
    }
  } catch (error) {
    onResponse.error(error);
    console.log("onResponse catch error " + error);
  }
}