export const BASE_URL = "https://e3c12e0b36c8.ngrok.io";

// Customer
export const FACEBOOK_LOGIN = "/api/v1/customers/facebook";
export const CUSTOMER_SIGNOUT = "/api/v1/customers/logout";
export const CUSTOMER_SHOW = '/api/v1/customers/show';
export const CUSTOMERS_PROFILE = "/api/v1/customers/update";
export const CUSTOMER_TRIPS = "/api/v1/customers/trips";
export const CUSTOMER_POSTULATE_TRIPS = "/api/v1/customers/trips/";
export const CUSTOMER_MESSAGES_LIST = "/api/v1/customers/trips/";
export const CUSTOMER_MY_TRIPS = "/api/v1/customers/my/trips";
export const CUSTOMER_PUSH_NOTIFICATION = "/api/v1/customers/add_mobile_token";

// Driver
export const DRIVER_SINGNIN = "/api/v1/drivers/login";
export const DRIVER_SIGNOUT = "/api/v1/drivers/logout";
export const DRIVER_SIGNUP = "/api/v1/drivers/signup";
export const DRIVER_CARS = "/api/v1/drivers/cars";
export const DRIVER_TRIPS = "/api/v1/drivers/trips";
export const DRIVER_PROVINCES = "/api/v1/drivers/provinces";
export const DRIVER_CREATE_TRIPS = "/api/v1/drivers/trips";
export const DRIVER_UPDATE = "/api/v1/drivers/update";
export const DRIVER_PUSH_NOTIFICATION = "/api/v1/drivers/add_mobile_token";