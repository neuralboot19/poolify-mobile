import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Home from '../components/home';
import CustomerDashboard from '../components/customer/Dashboard';
import CustomerUpdateProfile from '../components/customer/Profile';
import CustomerDetailsTrip from '../components/customer/DetailsTrip';

// Driver Screen
import DriverDashboard from '../components/driver/Dashboard';
import DriverUpdatePassword from '../components/driver/Password';
import DriverCreateTrip from '../components/driver/CreateTrip';
import DriverUpdateProfile from '../components/driver/Profile';
import DriverReviews from '../components/driver/Reviews';
import DriverCar from '../components/driver/ListCar';
import DriverDetailsTrip from '../components/driver/DetailsTrip';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="CustomerDashboard">
        {/* Navigator Globals */}
        <Stack.Screen name="Home" options={{ headerShown: false }} component={Home} />

        {/* Navigator Customer */}
        <Stack.Screen name="CustomerDashboard" options={{ headerShown: false }} component={CustomerDashboard} />
        <Stack.Screen name="CustomerUpdateProfile" options={{ title: 'Datos de emergencia', headerTransparent: true }} component={CustomerUpdateProfile} />
        <Stack.Screen name="CustomerDetailsTrip" options={{ title: 'Details trip', headerTransparent: true }} component={CustomerDetailsTrip} />

        {/* Navigator Driver */}
        <Stack.Screen name="DriverDashboard" options={{ headerShown: false }} component={DriverDashboard} />
        <Stack.Screen name="DriverUpdateProfile" options={{ title: 'Actualización del perfil', headerTransparent: true }} component={DriverUpdateProfile} />
        <Stack.Screen name="DriverReviews" options={{ title: 'Comentarios', headerTransparent: true }} component={DriverReviews} />
        <Stack.Screen name="DriverCars" options={{ title: 'Autos', headerTransparent: true }} component={DriverCar} />
        <Stack.Screen name="DriverUpdatePassword" options={{ title: 'Update Password', headerTransparent: true }} component={DriverUpdatePassword} />
        <Stack.Screen name="DriverCreateTrip" options={{ title: 'Crear Viaje', headerTransparent: true }} component={DriverCreateTrip} />
        <Stack.Screen name="DriverDetailsTrip" options={{ title: 'Details trip', headerTransparent: true }} component={DriverDetailsTrip} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;